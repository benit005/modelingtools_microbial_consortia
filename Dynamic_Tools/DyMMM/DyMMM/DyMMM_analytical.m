function [ tspan, y ] = DyMMM_analytical(tspan, init, community )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

numSpecies = length(community.species);
numMetabolites = length(community.mets);
delta_t = tspan(2)-tspan(1)

%initialize
Vspan(1) = init(1);                     
for i = 1:numSpecies
    Xspan(i,1) = init(1+i);           
end
for j = 1:numMetabolites
    Sspan(j,1) = init(1+numSpecies+j);  
end

for t = tspan(2:end)
t
    
% -----------------------------------------------
% Calculating fluxes from FBA    
vs =zeros(numSpecies,numMetabolites);
mu = zeros(1, numSpecies);

S = Sspan(:,end);
X = Xspan(:,end);
V = Vspan(:,end);
updateUptakes;  % updating FBA uptake constraints

% rxnListf = {'BIOMASS_SC5_notrace','BIOMASS_Ecoli'};

% calculating the growth rates and production/consumption rates of the organisms
for i = 1:numSpecies
    sol = optimizeCbModel(community.species(i),'max',false);
    if (sol.stat ~= 1)
        mu(i) = 0;
        sprintf('species %d is dying',i);
    else
       mu(i) = sol.x(findRxnIDs(community.species(i),'BIOMASS'));
%          mu(1) = sol.x(findRxnIDs(community.species(1),'BIOMASS_SC5_notrace'));
%          mu(2) = sol.x(findRxnIDs(community.species(2),'BIOMASS_Ecoli'));
        for j = 1:numMetabolites
            if ismember(community.mets(j),community.species(i).rxns)
                vs(i,j) = sol.x(findRxnIDs(community.species(i),community.mets(j)));
            end
        end
    end
end
% -------------------------------------------------

updateEnvironment;  %updating environmental parameters such as flow rates and feed concentration

Vo = V;
Xo = X;
So = S;

% Calculating new volume
Vf = Vo + (community.Fin - community.Fout)*delta_t;

% Calculating new biomass concentration due to growth and dilution
Xf = Xo.*exp((mu'-community.Fin/Vo).*delta_t);

% Calculating new biomass due to feeding
Xf = Xf + (community.Fin.*community.Xfeed/Vo*delta_t)';

delta_X = Xf - Xo;


% Calculating new metabolite concentration
testVar = Vo./(mu.*Vo-community.Fin).*delta_X'; 
testVar(isnan(testVar))=0;  %ensures the correct solution for zero growth situations where V/(mu-Fin)= V/0 = InF
metabolism = (testVar*vs)';  %metabolic production and consumption of S
abiotic = community.Fin.*(community.Sfeed'-So)/Vo*delta_t; %S change due to environmental flow
Sf = So + metabolism + abiotic;

% Non-negative approximation
Vf(Vf<0)=0;
Xf(Xf<0)=0;
Sf(Sf<0)=0;

Vspan(:,end+1) = Vf;
Xspan(:,end+1) = Xf';
Sspan(:,end+1) = Sf;

end

size(Vspan)
size(Xspan)
size(Sspan)

y = [Vspan;Xspan;Sspan]';

end

