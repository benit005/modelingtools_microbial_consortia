function [ t, V, X, S ] = DyMMM ( community, tspan, initialConditions, solvers )
%dymmm: dynamic multispecies metabolic modeling framework
%   community:  description of the microbial community and its environment
%       community.species:  species in the community
%       community.mets:     metabolites to be tracked by DMMM
%       community.Xfeed:    biomass in the feed (g/L)
%       community.Sfeed:    metabolites in the feed (mmol/L)
%       community.Fin:      flow rate in (L/hr)
%       community.Fout:     flowrate out (L/hr)
%   tspan: simulation time
%   intialConditions: the initial reactor volume, biomass and metabolite concentrations
%   solvers: 
%       'ode45', 'ode45n','ode23','ode23n','analytical'
%       
%       Analytical Solution: "analytical"
%       MATLAB ODE Solvers: "ode45", "ode23"   
%       MATLAB ODE solvers with "NonNegative Flag set: "ode45n", "ode23n"
%       The suffix "n" signifies that the "NonNegative" flag of MATLAB ODE solvers is turned on
%       
%       Note: Do not use the analytical solution method if Xfeed>0.

numSpecies = length(community.species);
numMetabolites = length(community.mets);
    
switch solvers
    case 'ode45'
        [t,y] = ode45('DyMMM_ode',tspan,initialConditions,[], community)
    case 'ode45n'
        abc = 1:length(initialConditions);
        options = odeset('NonNegative',abc);
        [t,y] = ode45('DyMMM_ode',tspan,initialConditions,options, community)
    case 'ode23'
        [t,y] = ode45('DyMMM_ode',tspan,initialConditions,[], community)
    case 'ode23n'
        abc = 1:length(initialConditions);
        options = odeset('NonNegative',abc);
        [t,y] = ode45('DyMMM_ode',tspan,initialConditions,options, community)
    case 'analytical'
        [t,y] = DyMMM_analytical(tspan, initialConditions,community);
    otherwise
        disp "DMMM does not recognize the ODE solver selected."
end

V = y(:,1);
X = y(:,2:1+numSpecies);
S = y(:,2+numSpecies:end);

end

