% Updates the uptake rates of the organisms based on M
% This function is called in DMMM2 prior to FBA calculation
for i = 1:numSpecies
    switch community.species(i).description
		case 'Glucose_user'			
			% glucose
            [TF, loc] = ismember('EX_glc(e)',community.mets);
            C = S(loc)/V;
            Vmax_glc = -25.9*C/(C + 0.5);  %Assume Ks on glucose same as S. cerevisiae
			community.species(i) = changeRxnBounds(community.species(i),'EX_glc(e)',[Vmax_glc],'l');
			
			% xylose
            [TF, loc] = ismember('EX_xyl_D(e)',community.mets);
            C = S(loc)/V;
            Vmax_xyl = 0;  %Assume Ks on glucose same as S. cerevisiae
			community.species(i) = changeRxnBounds(community.species(i),'EX_xyl_D(e)',[Vmax_xyl],'l');
         
            % ethanol
            [TF, loc] = ismember('EX_etoh(e)',community.mets);
            C = S(loc)/V;
            Vmax_etoh = 0;  %Assume Ks on glucose same as S. cerevisiae
			community.species(i) = changeRxnBounds(community.species(i),'EX_etoh(e)',[Vmax_etoh],'l');
            
             % oxygen
            [TF, loc] = ismember('EX_o2(e)',community.mets);
            C = S(loc)/V;
            Vmax_oxy = -1.5*C/(C + 0.003);  %Assume Ks on glucose same as S. cerevisiae
			community.species(i) = changeRxnBounds(community.species(i),'EX_o2(e)',[Vmax_oxy],'l');
            
            
		case 'Xylose_user'			
			% glucose
            [TF, loc] = ismember('EX_glc(e)',community.mets);
            C = S(loc)/V;
            Vmax_glc = 0; %-10*C/(C + 0.5);  %Assume Ks on glucose same as E. coli
			community.species(i) = changeRxnBounds(community.species(i),'EX_glc(e)',[Vmax_glc],'l');
			
			% xylose
            [TF, loc] = ismember('EX_xyl_D(e)',community.mets);
            C = S(loc)/V;
            Vmax_xyl = -9*C/(C + 0.01);  %Assume Ks on glucose same as E. coli
			community.species(i) = changeRxnBounds(community.species(i),'EX_xyl_D(e)',[Vmax_xyl],'l');
            
            % ethanol
            [TF, loc] = ismember('EX_etoh(e)',community.mets);
            C = S(loc)/V;
            Vmax_etoh = 0;  %Assume Ks on glucose same as E. coli
			community.species(i) = changeRxnBounds(community.species(i),'EX_etoh(e)',[Vmax_etoh],'l');
            
            % oxygen
            [TF, loc] = ismember('EX_o2(e)',community.mets);
            C = S(loc)/V;
            Vmax_oxy = -8*C/(C + 0.001);  %Assume Ks on glucose same as E. coli
			community.species(i) = changeRxnBounds(community.species(i),'EX_o2(e)',[Vmax_oxy],'l');
    end	
end