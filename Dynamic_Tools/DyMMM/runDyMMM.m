clear all;

%  Initialize COBRA Toolbox
initCobraToolbox;

    % set the variable "model" to a COBRA Toolbox model
    load iND750.mat   %load S. cerevisiae iND750
        %modelY = iMM904;             %Setup model to iMM904
    load iJR904.mat   %load E. coli iJR904
        %modele = iJR904;             %Setup model to iJR904
        
        model1 = iND750;
        model1.description = 'Glucose_user';
        model2 = iJR904;
        model2.description = 'Xylose_user';
        
% model1 = changeRxnBounds(model1, 'ATPM', 1, 'b');
% model2 = changeRxnBounds(model2, 'ATPM', 7.6, 'b');
% model1.lb(strcmp(model1.rxns,'EX_o2(e)')) = -1000;        %O2
% model2.lb(strcmp(model2.rxns,'EX_o2(e)')) = -1000;    %O2
% model1.lb(strcmp(model1.rxns,'EX_glc(e)')) = -1000;    %	Glucose
model2.lb(strcmp(model1.rxns,'GLUK')) = 0;%	Glucose kinase
% model2.lb(strcmp(model2.rxns,'EX_xyl_D(e)')) = -1000;    %	Xylose
% model1.lb(strcmp(model1.rxns,'EX_etoh(e)')) = -1000;    %	Ethanol
% model2.lb(strcmp(model2.rxns,'XYLt2')) = -10;    %	D-xylose transport in via proton symport
% model2.lb(strcmp(model2.rxns,'XYLabc')) = -10;    %	D-xylose transport via ABC system
% model2.lb(strcmp(model2.rxns,'XYLI1')) = -10;    %	D-xylose transport via ABC system
model2 = changeRxnBounds(model2,'EX_glc(e)',0,'b');
% model1 = changeCOBRAVariable(model1,'BIOMASS_SC5_notrace','BIOMASS');
% model2 = changeCOBRAVariable(model2,'BIOMASS_Ecoli','BIOMASS'); 

community.species = [model1 model2];
community.mets = {'EX_glc(e)','EX_xyl_D(e)','EX_etoh(e)','EX_o2(e)'};	%metabolites tracked by DMMM
community.Xfeed= [0 0];							%biomass feed concentration
community.Sfeed = [0 0 0 0];						%substrate feed concentration

community.Fin = 0;      %dilution into the reactor
community.Fout = 0;     %dilution out of the reactor

initV = 1; % %L
initX = [0.05 0.05]; %g
initS = [88.8 53.28 0 0.24];   %mmol

initialConditions = [initV initX initS];


tspan = [0:0.02:14];
[t, V, X, S] = DyMMM(community, tspan, initialConditions, 'analytical');    

figure;
subplot(3,1,1);plot(t,V);title('DyMMM simulation using analytical solution');ylabel('Volume');
subplot(3,1,2);plot(t,X);ylabel('Biomass');
subplot(3,1,3);plot(t,S);ylabel('Metabolites');

tspan = [0:0.2:14];
[t, V, X, S] = DyMMM(community, tspan, initialConditions, 'ode45');    

figure;
subplot(3,1,1);plot(t,V);title('DyMMM simulation using ode45 solution');ylabel('Volume');
subplot(3,1,2);plot(t,X);ylabel('Biomass');
subplot(3,1,3);plot(t,S);ylabel('Metabolites');