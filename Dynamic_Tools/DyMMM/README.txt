**Dynamic Multi-species Metabolic Modelling (DyMMM) framework**
Version	:	1.3
Author	:	Kai Zhuang (updated by William Scott)
Platform:	MATLAB, COBRA Toolbox

--------
OVERVIEW 
--------
The Dynamic Multispecies Metabolic Modeling framework is a mathematical modeling tool that integrates multiple constraint-based metabolic models into a single dynamic community metabolic model.  The DyMMM framework is designed to model competitive and syntrophic (cross-feeding) communities.  It *should not be used* to model mutualistic communities.

The base mathematics of DyMMM was first published by Zhuang *et al.* (2011).  The system of equations can be solved either numerically, or through an analytical approximation method.  Since the initial publication, the mathematics of DyMMM has expanded to include additional capabilities.  For the most update-to-date mathematics, see the SourceForge Wiki Page.

Please use the following citation for bibliographical purposes: 
Zhuang, K., Izallalen, M., Mouser, P., Richter, H., Risso, C., Mahadevan, R., & Lovley, D. R. (2011). Genome-scale dynamic modeling of the competition between Rhodoferax and Geobacter in anoxic subsurface environments. The ISME journal, 5(2), 305–316. doi:10.1038/ismej.2010.117 

Zhuang, K., Ma, E., Lovley, D. R., & Mahadevan, R. (2012). The design of long-term effective uranium bioremediation strategy using a community metabolic model. Biotechnology and Bioengineering. doi:10.1002/bit.24528

------------------------------
How To Use the DyMMM framework
------------------------------
The DMMM framework contains six files:

- dymmm.m						//main function

- dymmm_ode.m					//ODE description of a microbial community and its environment	

- dymmm_analytical.m					//analytical solution to DyMMM

- updateEnvironment.m			//description of the environmental changes

- updateUptakes.m				//description of the microbial uptake kinetics

- runDyMMM.m					//a sample script that sets up and runs the simulation

To set up a simulation:
1. The first three files (dymmm.m, dymmm_ode.m, dymmm_analytical) does not need to be modified.

2. The environmental conditions and the uptake constraints of the metabolic models are updated at every time step using the updateEnvironment and updateUptakes.m scripts.
	- updateEnvironment.m only needs to be modified if the environment is expected to be changed during the simulation.  eg. substrate is added in the middle of the simulation.
	- updateUptakes.m is necessary.  a new "case block" is required for each new species.  
	- conditionals (if/else) commands can be used within these two files to establish very complex simulations.  for example, u can specify one uptake kinetic equation if glucose is available, and another uptake kinetic equation if it is not available. 

3. runDMMM.m gives a good example of how to set up a simulation using two hypothetical E.coli strains, simulating a cross-feeding situation where one strain consumes glucose and produces acetate, whereas the other strain consumes acetate.
	In brief, you need to:
	- Setup the community 
		- species
		- metabolites to be tracked
		- biomass feed concentration (if there's any biomass diluted into the system)
		- substrate feed concentration
		- flow rate (in and out)

	- Setup timespan (tspan)

	- Setup initial conditions
		- biomass concentrations
		- substrate concentrations
		- reactor liquid volume

	- Choose a solver
                - "analytical" for analytical approximation.  it is important to set the time step to be relatively small. in addition, this is not appropriate for situations where biomass is being fed into the system.
                - "ode45","ode45n","ode23","ode23n" for numerical solvers.  "n" signifies that the non-negative flag is turned on.

	- run the simulation using the function dymmm

4. Solutions:
	y(1) is community volume
	y(2) to y(1+numSpecies) are the biomass of the organisms
	y(2+numSpecies) to y(end) are the concentration of metabolites
	
	numSpecies is the number of microbial species