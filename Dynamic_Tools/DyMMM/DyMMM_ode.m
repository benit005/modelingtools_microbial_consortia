function dy = DyMMM_ode(t, y, flag, community)
% ODEs  of DMMM framework
% y(1) is community volume
% y(2) to y(1+numSpecies) are the biomass of the organisms
% y(2+numSpecies) to y(end) are the concentration of metabolites
t
%y(y<0)=0;

dy = zeros(size(y));
    numSpecies = length(community.species);
    numMetabolites = length(community.mets);

V = y(1);                     %volume [L]
for i = 1:numSpecies
    X(i) = y(1+i);              %biomass [g]
end
for j = 1:numMetabolites
    S(j) = y(1+numSpecies+j);  %substrate [mol]
end

% assigning growth rates and metabolic production/consumption rates
    % here, the rates are calculated using FBA through COBRA Toolbox
    % these rates can be calculated and assigned using other methods
    % for example, a Michaelis-Menten kinetic expression can be used

vs =zeros(numSpecies,numMetabolites);
mu = zeros(1, numSpecies);

updateUptakes;  % updating FBA uptake constraints

% calculating the growth rates and production/consumption rates of the organisms
for i = 1:numSpecies
    sol = optimizeCbModel(community.species(i),'max',false);

    if (sol.stat ~= 1)    
        mu(i) = 0;
        disp('deathphase');
    else
        mu(i) = sol.x(findRxnIDs(community.species(i),'BIOMASS'));
        for j = 1:numMetabolites
            if ismember(community.mets(j),community.species(i).rxns)
                vs(i,j) = sol.x(findRxnIDs(community.species(i),community.mets(j)));
            end
        end
    end
end

updateEnvironment;  %updating environmental parameters such as flow rates and feed concentration

% calculating the rates of change of reactor volume[L], biomass [g/L] and metabolite [mmol/L]
dy(1) = community.Fin - community.Fout; %dV/dt [L/hr]
dy(2:1+numSpecies) =  mu.*X + community.Fin/V*(community.Xfeed - X); %dX/dt [g/L/hr]
dy(2+numSpecies:end) = X*vs + community.Fin/V*(community.Sfeed - S); %dS/dt [mmol/L/hr]
end
