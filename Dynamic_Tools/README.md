# Dynamic Tools

Similar to what was done by Hanly and coworkers , Saccharomyces cerevisiae S288C
(iND750) and Escherichia coli K-12 substr. MG1655’s (iJR904) GEMs, 
acquired from the BiGG database , were used to perform the model simulations 
with DyMMM, DFBAlab, MMODES, and μbialSim. Furthermore, the E. coli 
model(iJR904) was modified by constraining flux bounds for glucose exchange and
glucose kinase at zero to mathematically reflect the associated gene deletions. The 
simulations were based on an aerobic xylose co-culture of S. cerevisiae and the 
engineered E. coli strain ZSC113 fermentation experiment. In this experiment, glucose 
and xylose concentrations are expected to decrease over time, while the ethanol 
concentration is expected to increase. Simultaneously, S. cerevisiae and E. coli biomass
concentrations are expected to increase over time. For all simulations, any constraints 
and parameters given in the original tools were modified according to the experimental 
values from the respective dataset from Hanly and coworkers. 

The corresponding results, GEMs and scripts to run each dynamic tool are found in each tool's folder. Each script specifies the software version used to run the tools and are well documented. Every tool has a folder containing the input GEMs. In addition, every tool contains a specific README file with instructions on how to operate and run the tools as were performed in this study.  
