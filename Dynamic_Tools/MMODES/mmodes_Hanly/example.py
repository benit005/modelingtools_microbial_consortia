#!/usr/bin/python3

# Example script for mmodes package.
# Author: Jorge Carrasco Muriel
# e-mail: jorge.cmuriel@alumnos.upm.es

import json
import mmodes

def main():
    with open("../mmodes_Hanly/ModelsInput/media.json") as jdata:
        media = json.load(jdata)[0]
    # 1) instantiate Consortium
    # if "manifest" contains a non-empty string, it will generate COMETS-like ouput
    cons = mmodes.Consortium(stcut = -1e-8, mets_to_plot = ["glc__D_e", "xyl__D_e", "etoh_e", "o2_e"], v = 1, manifest = "COMETS_manifest.txt")
    # 2) instantiate dMetabolites
    # for instance, https://www.ncbi.nlm.nih.gov/pubmed/18791026?dopt=Abstract
    glc = mmodes.dMetabolite(id = "glc__D_e", Km = 0.5, Vmax = 25.9)
    xyl = mmodes.dMetabolite(id = "xyl__D_e", Km = 0.01, Vmax = 9)
    o2 = mmodes.dMetabolite(id = "o2_e", Km = 0.001, Vmax = 8)
    # 3) add model
    # model from AGORA or a BIGG model. A single strain to agilize example.
    cons.add_model("../mmodes_Hanly/ModelsInput/iND750.xml", 0.05, solver = "glpk", method = "fba", dMets = {glc.id: glc, o2.id: o2}) # iND750.xml (S. cerevisiae) (euavg)
    cons.add_model("../mmodes_Hanly/ModelsInput/iJR904.xml", 0.05, solver = "glpk", method = "fba", dMets = {xyl.id: xyl, o2.id: o2})  # iJR904.xml (E. coli) (euavg)
    # 4) instantiate media
    cons.media = cons.set_media(media, True)
    # 5) run it, plotting the output
    cons.run(maxT = 14, outp = "plot_example.png", outf = "tab_example.tsv", verbose=True, integrator = 'fea', stepChoiceLevel = (0.02, 0, 1000))
    # 6) print stuff on screen
    for mod in cons.models:
        print(mod, cons.models[mod].volume.q, sep = " -> ")
    print("Glucose", str(cons.media["glc__D_e"]), sep = " -> ")
    print("Xylose", str(cons.media["xyl__D_e"]), sep = " -> ")
    print("Ethanol", str(cons.media["etoh_e"]), sep=" -> ")
    print("Oxygen", str(cons.media["o2_e"]), sep=" -> ")
    print()

if __name__ == "__main__":
    main()
