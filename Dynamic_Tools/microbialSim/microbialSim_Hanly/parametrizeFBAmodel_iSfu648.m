function [ model ] = parametrizeFBAmodel_iSfu648Model(model)
%PARAMETRIZE Setting the parameters (NGAM and uptake kinetics)for the yeast (iND750)model
%   Detailed explanation goes here

%% NGAM

NGAMValue = 1; % mmolATP/gDW/h

switch model.FBAsolver
    case 1
        model.CNAconstraints(model.ngamReac) = NGAMValue;
    case 2
        model.COBRAmodel.lb(model.ngamReac) = NGAMValue;
        model.COBRAmodel.ub(model.ngamReac) = NGAMValue;
    otherwise
       display('parametrizeModel(): unkown type selected')
end

%% UPTAKE KINETICS
%Glu, Xyl, Etoh, O2
model.coupledReactions.vmax = [25.9 0 0 1.5]';
model.coupledReactions.ks = [0.5 0 0 0.003]';

