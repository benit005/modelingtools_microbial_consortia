function [ model ] = parametrizeFBAmodel_iMhu428(model)
%PARAMETRIZE Setting the parameters (NGAM and uptake kinetics)for the E. coli (iJR904) model
%   Detailed explanation goes here

%% NGAM

NGAMValue = 1; % mmolATP/gDW/h

switch model.FBAsolver
    case 1
        model.CNAconstraints(model.ngamReac) = NGAMValue;
    case 2
        model.COBRAmodel.lb(model.ngamReac) = NGAMValue;
        model.COBRAmodel.ub(model.ngamReac) = NGAMValue;
    otherwise
       display('parametrizeModel(): unkown type selected')
end

%% UPTAKE KINETICS
% Glu, Xyl, EtOH, O2
model.coupledReactions.vmax = [0 9 0 8]';
model.coupledReactions.ks = [0 0.01 0 0.001]';

