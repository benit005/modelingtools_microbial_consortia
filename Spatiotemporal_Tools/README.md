# Spatiotemporal Tools

Similar to what was done by Harcombe et al., simulations were performed using the
GEMs for E. coli (iJO1366) and S. enterica (iRR1083). We compared 
BacArena, IndiMeSH, and CROMICS, which were designed to study microbial
communities using FBA and IBMs, with COMETS which was mainly designed to 
investigate the interrelationships of bacterial communities in space using FBA and 
PLMs. The goal was to compare the methods with one another for their potential in 
simulating trophic dependences of multispecies bacterial communities without making 
prior assumptions. Therefore, simulations mimicked the two-member consortium 
experiment studied in the original publication. This consortium was composed of 
two mutant strains, S. enterica LT2 and E. coli K-12. S. enterica LT2 cannot 
metabolize and consume lactose, and E. coli K-12 cannot produce methionine. Hence,
these two species participate in a mutualistic relationship because S. enterica relies on 
the secretion of acetate for a substrate by E. coli while E. coli needs S. enterica to 
produce methionine. Stoichiometric models of each species were modified to incorporate
known genetic constraints according to the method of Harcombe and coworkers. 
For instance, in the E. coli strain, the metB mutation was accounted for by constraining 
to zero the flux through the corresponding reaction (cystathionine γ-synthase). In S.
enterica, methionine necessitated that we added a gain-of-function mutation in metA
(homoserine transsuccinylase). This secretion was modeled as coupled to biomass where 
lactose is utilized by E. coli, so that as cells grew, they produced appropriate amino 
acid levels. A summary of boundary conditions, constraints, and parameters for all 
GEMS for spatiotemportal methods can be found in the supplementary material

The corresponding results, GEMs, and scripts to run each dynamic tool are found in each tool's folder. Each script specifies the software version used to run the tools and are well documented. Every tool has a folder containing the input GEMs. In addition, every tool contains a specific README file with instructions on how to operate and run the tools as were performed in this study.  