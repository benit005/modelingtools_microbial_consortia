function [] = preSimulationAnalysis_FBAIBM( handles )
%PRESIMULATIONANALYSIS Summary of this function goes here
%   Detailed explanation goes here






%%% Physical analysis






%%% Chemical analysis






%%% Biological analysis
for ns=1:handles.bio.nspecies
    bac=handles.bio.species{1};
    [ T ] = metNetworkAnalysis_FBAIBM( bac,substrateNames );
    filename=[handles.paths.outputPath handles.adm.sep bac.genus '_' bac.name '_' bac.metabolicNetwork '_MetabolicAnalysis.txt'];
    writetable(T,filename)
end






end

