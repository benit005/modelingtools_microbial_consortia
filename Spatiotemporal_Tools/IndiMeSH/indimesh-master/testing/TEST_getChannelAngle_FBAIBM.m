% TEST ROUTINE:
% Returns the angle of the channel depending on the change in X and change
% in Y, in future, needs to be generalised for different networks
%
%INPUT
% changeX       Change in X from node 1 to node 2 [1 x 1]
% changeY       Change in X from node 1 to node 2 [1 x 1]
%
%OPTIONAL INPUT
% []            No optional input required  
%                
%OUTPUTS
% angle1        angle in horizontal space
% angle2        angle in vertical space
%
% Benedict Borer 10/2016
% BB, 10/2016, Created file
% BB, 05/2017, Changed file to include vertical space


% test for a horizontal line towards X
changeX=1;
changeY=0;
changeZ=0;
[ angle1, angle2 ] = getChannelAngle_FBAIBM( changeX,changeY, changeZ );
expectedAngle1=0;
expectedAngle2=0;
assertAbsTol_FBAIBM(10^-6,angle1,expectedAngle1,'Wrong calculation of horz angle')
assertAbsTol_FBAIBM(10^-6,angle2,expectedAngle2,'Wrong calculation of vert angle')

% 90� to y
changeX=0;
changeY=1;
changeZ=0;
[ angle1, angle2 ] = getChannelAngle_FBAIBM( changeX,changeY, changeZ );
expectedAngle1=pi/2;
expectedAngle2=0;
assertAbsTol_FBAIBM(10^-6,angle1,expectedAngle1,'Wrong calculation of horz angle')
assertAbsTol_FBAIBM(10^-6,angle2,expectedAngle2,'Wrong calculation of vert angle')

% vertical line (90� z)
changeX=0;
changeY=0;
changeZ=1;
[ angle1, angle2 ] = getChannelAngle_FBAIBM( changeX,changeY, changeZ );
expectedAngle1=0;
expectedAngle2=pi/2;
assertAbsTol_FBAIBM(10^-6,angle1,expectedAngle1,'Wrong calculation of horz angle')
assertAbsTol_FBAIBM(10^-6,angle2,expectedAngle2,'Wrong calculation of vert angle')

% 45� in z, 90� in y
changeX=0;
changeY=1;
changeZ=1;
[ angle1, angle2 ] = getChannelAngle_FBAIBM( changeX,changeY, changeZ );
expectedAngle1=pi/2;
expectedAngle2=pi/4;
assertAbsTol_FBAIBM(10^-6,angle1,expectedAngle1,'Wrong calculation of horz angle')
assertAbsTol_FBAIBM(10^-6,angle2,expectedAngle2,'Wrong calculation of vert angle')
