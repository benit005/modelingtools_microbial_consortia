% TEST ROUTINE:
% Calculates the angularity factor based on the number of corners
%
%INPUT
% ncorners          number of polygon corners or sides 
%
%OPTIONAL INPUT
% []                No optional input required  
%                
%OUTPUTS
% angularityFactor  Angularity factor (-)  
%
% Benedict Borer 05/2017
% BB, 05/2017, Created file


% Test for a rectangle
prepAngle=[90 90 90 90];
expectedResult=4-pi;
[ angularityFactor ] = calculateAngularityFactor_FBAIBM(prepAngle);
assertAbsTol_FBAIBM(10^-6,angularityFactor,expectedResult,'Wrong calculation of angularity factor for rectangle')

% Test for a regular triangle
prepAngle=[60 60 60];
expectedResult=3*sqrt(3)-pi;
[ angularityFactor ] = calculateAngularityFactor_FBAIBM(prepAngle);
assertAbsTol_FBAIBM(10^-6,angularityFactor,expectedResult,'Wrong calculation of angularity factor for triangle')











