function assertAbsTol_FBAIBM(tolerance,calculatedVal,trueValue,varargin)
% Compares a calculated value to an expected using absolute tolerance
%
%INPUT
% tolerance         tolerance of comparison
% calculatedVal     calculated value
% trueValue         expected value
% carargin          string containing error message
%
%OPTIONAL INPUT
% []                No optional input required  
%                
%OUTPUTS
%   
%
% Benedict Borer 05/2017
% BB, 05/2017, Created file

testValue = abs(calculatedVal-trueValue) <= tolerance;
assert(testValue, varargin{:});
end