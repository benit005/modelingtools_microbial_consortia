% TEST ROUTINE
% Calculates the median distance between connected points
%
%
%INPUT
% pts               Coordinates of the nodes [X Y]
% adj               Adjacency matrix [npts x npts]
%
%OPTIONAL INPUT
% []                No optional input required
%
%OUTPUTS
% channelLength     Vector containing the length of each channel [nlnks x 1]
%
% Benedict Borer 10/2016
% BB, 10/2016, Created file


X=[0 1 1 0]';
Y=[0 0 1 1]';
adj=[0 1 0 1;
    1 0 1 0;
    0 1 0 1;
    1 0 1 0];
pts=[X,Y];

[channelLength ] = getNetworkMesh_FBAIBM( pts,adj );
expectedValue=1;
assertAbsTol_FBAIBM(10^-6,channelLength,expectedValue,'Wrong calculation network mesh')



X=[0 0.1 0.1 0]';
Y=[0 0 0.1 0.1]';
adj=[0 1 0 1;
    1 0 1 0;
    0 1 0 1;
    1 0 1 0];
pts=[X,Y];

[channelLength ] = getNetworkMesh_FBAIBM( pts,adj );
expectedValue=0.1;
assertAbsTol_FBAIBM(10^-6,channelLength,expectedValue,'Wrong calculation network mesh')







