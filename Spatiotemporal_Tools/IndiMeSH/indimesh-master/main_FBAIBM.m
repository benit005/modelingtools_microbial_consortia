function [] = main_FBAIBM(modelPath, filePath, metabolicPath, saveStump, hydrationID,inoculationID, SimulationName, ParalellOption, SolverName, videoOption)
% This file schedules preparation of the model structure, loading of all
% data and finally running the actual iteration loop
%
%
%INPUT
% modelPath         FBA-IB model struct    
% filePath          Path to the simulation folder
% saveStump         String stump for saving data
% hydrationID       ID of the hydration file (which realisation)
% inoculationID     ID of the inoculation file (which realisation)
% SimulationName    Name of the simulation
% ParalellOption    Either paralellisation of growth or simulations
% SolverName        Name of the solver to be used for FBA/TFA
% videoOption       Option to save video data, 1 mean take data
%
%OPTIONAL INPUT
% []                No optional input required  
%                
%OUTPUTS
% []                No output, data is saved 
%
% Benedict Borer 04/2017
% BB, 04/2017, Created file, adapted
% BB, 11/2018, Added video option


% Create the model structure, add paths and check them
[ handles ] = prepareModelPaths_FBAIBM( modelPath, filePath, metabolicPath,saveStump, hydrationID, inoculationID, SimulationName, ParalellOption, SolverName, videoOption);

% Initialise simulation
[ handles ] = loadInitialisationFiles_FBAIBM(handles);

% Inoculate bacteria
[ handles ] = inoculateBacteria_FBAIBM(handles);

cd(handles.paths.outputPath)
concfilename = [saveStump num2str(ceil(handles.temp.t./3600),'%02d') '_initial'];
save(concfilename)
cd(handles.paths.modelPath)

% Run simulation
[ handles ] = simulationLoop_FBAIBM(handles);

cd(handles.paths.outputPath)
concfilename = [saveStump num2str(ceil(handles.temp.t./3600),'%02d') '_final'];
save(concfilename)
cd(handles.paths.modelPath)

end



