function [ A ] = getDiffusionMatrix_FBAIBM( adj,r, area,volume, convertAdj )
% Creates the diffusion matrix A of the diffusion equation Ax=b, no
% boundary conditions yet implemented!
%
%INPUT
% adj               adjacency matrix
% r                 retardation coefficient (D*dt/x)
% area              diffusive area of each node
% volume            Volume at nodes
%
%OPTIONAL INPUT
% []                No optional input required  
%                
%OUTPUTS
% area              Diffusive matrix A
%
% Benedict Borer 05/2017
% BB, 05/2017, Created file
% BB, 04/2018, Corrected a mistake in the volume (2*V)

%calculate coefficient of matrices
[row,col,~]=find(adj);
[npts,~]=size(adj);


sparseDivision=sparse(row,col,area(convertAdj)./(volume(row)),npts,npts);
% sparseDivision=sparse(row,col,area(convertAdj)./(volume(convertAdj)),npts,npts);

% Create values at all places except diagonal
sparseCoefficient=r.*sparseDivision;

% Create diagonal as sum of satelites + 1
sumCoefficient=full(sum(sparseCoefficient,2));
diagSumCoefficient=spdiags(sumCoefficient,0,npts,npts);

% Create the final diffusion matrix
A=-sparseCoefficient+diagSumCoefficient+speye([npts,npts]);
end
