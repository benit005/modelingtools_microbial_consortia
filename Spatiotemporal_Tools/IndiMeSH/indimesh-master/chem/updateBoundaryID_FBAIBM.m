function [ boundary_ID ] = updateBoundaryID_FBAIBM( boundaryNodes, boundaryGroups, Cb, iteration, nsubstrates )
% Update the ID of boundary nodes, determining if they are sources or no
% flux
%
%
%INPUT
% boundaryNodes     vector containing node ID of all boundary nodes
% boundaryGroups    cell with all boundary groups
% Cb                chemical boundary conditions
% substrateID       integer specifying the substrate
%
%OPTIONAL INPUT
% []                No optional input required  
%                
%OUTPUTS
% boundary_ID       vector with boundary ID's for each substrate
%                   1 = dead end (no flux)
%                   2 = constant concentration (source if positive / sink if 0)
%          
%
% Benedict Borer 10/2016
% BB, 10/2016, Created file


boundary_ID=ones(length(boundaryNodes),nsubstrates)*-1;
for kk=1:length(Cb)
    boundary=Cb{kk};
    concSpec=boundary(iteration,:);
    [flag,id]=ismember(boundaryGroups{kk},boundaryNodes);
    boundary_ID(id,:)=repmat(concSpec,length(id),1);
end

boundary_ID(boundary_ID>=0)=2;
boundary_ID(boundary_ID<0)=1;
end

