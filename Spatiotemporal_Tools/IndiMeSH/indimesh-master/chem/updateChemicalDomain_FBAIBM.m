function [ handles ] = updateChemicalDomain_FBAIBM( handles, varargin )
% Update the chemical domain including
%
%
%INPUT
% handles           IndiMeSH model structure
%
%OPTIONAL INPUT
% []                No optional input required
%
%OUTPUTS
% handles           IndiMeSH model structure

% Benedict Borer 10/2016
% BB, 10/2016, Created file
% BB, 03/2018, Changed the calculation of the laplacian using sub time step

narginchk(1,2);

if nargin==1
   iteration=1; 
else
   iteration=varargin{1};
end

% Add the desaturated channels to the boundary representing air
invadedNodes=unique(reshape(handles.geom.links.connectedPts(handles.hydro.saturation<1,:),[],1));
for ns=1:length(handles.geom.boundaryGroupsOriginal)
    %If boundary classification = 1, then its an air boundary
    if handles.geom.boundaryClassification(ns)
        boundary=handles.geom.boundaryGroupsOriginal{ns};
        boundary=unique([boundary; invadedNodes]);
        handles.geom.boundaryGroups{ns}=boundary;
    else
        handles.geom.boundaryGroups{ns}=handles.geom.boundaryGroupsOriginal{ns};
    end
end

% Check if any of the boundaries are air boundaries
if any(handles.geom.boundaryClassification)
    handles.geom.boundaryNodes=unique([handles.geom.boundaryNodesOriginal;invadedNodes]);
else
    handles.geom.boundaryNodes=unique([handles.geom.boundaryNodesOriginal]);
end
boundaryIDmat=updateBoundaryID_FBAIBM( handles.geom.boundaryNodes,handles.geom.boundaryGroups, handles.chem.Cb, iteration, handles.chem.nsubstrates);
handles.chem.boundaryID=boundaryIDmat;

% Create Laplacian for Numerical diffusion
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
laplacian=cell(handles.chem.nsubstrates,1);
for ns=1:handles.chem.nsubstrates
    laplacian{ns}=updateLaplacian_FBAIBM( handles.geom.adj,handles.hydro.diffAreaWater, handles.hydro.volumeWater, handles.chem.DWater(ns), handles.temp.dt, handles.geom.links.length,...
        boundaryIDmat(:,ns),handles.geom.boundaryNodes, handles.geom.convertAdj);
end

% for ns=1:handles.chem.nsubstrates
%     laplacian{ns}=updateLaplacian_FBAIBM( handles.geom.adj,handles.hydro.diffAreaWater, handles.hydro.volumeChannel, handles.chem.DWater(ns), handles.temp.dt, handles.geom.links.length,...
%         boundaryIDmat(:,ns),handles.geom.boundaryNodes, handles.geom.convertAdj);
% end

handles.chem.laplacian=laplacian;
end

