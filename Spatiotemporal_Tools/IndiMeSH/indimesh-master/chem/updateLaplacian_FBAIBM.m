function [ A ] = updateLaplacian_FBAIBM( adj,area,volume, D, dt, channelLength, boundaryID,boundaryNodes,convertAdj )
% Update the laplacian for substrate diffusion
%
%
%INPUT
% adj               Adcacency matrix of the network
% area              Diffusive area in channels
% volume            Volume at nodes
% D                 Diffusion coefficient
% dt                Time step
% channelLength     Channel length, either a single number or vector
% boundaryID        Vector of length boundaryNodes, ID for each boundary
% boundaryNodes     Vector with node ID's of boundaries
% degreeSparse      Sparse diagonal matrix containing the degree of each node
%
%OPTIONAL INPUT
% []                No optional input required  
%                
%OUTPUTS
% A                 Diffusive matrix with boundary conditions
%
%
% Benedict Borer 10/2016
% BB, 10/2016, Created file
% BB, 05/2017, Changed calculation to mass balance from concentration
% balance

r=(D.*dt)./((channelLength));
[ A ] = getDiffusionMatrix_FBAIBM( adj,r, area,volume,convertAdj );


% for i=1:length(boundaryID)
%     if boundaryID(i)==2 % Source or sink (Dirichlet)
%         A(boundaryNodes(i),:)=0;
%         A(boundaryNodes(i),boundaryNodes(i))=1;
% %     elseif boundaryID(i)==1 % No flux boundary
% %         [~,col]=find(adj(boundaryNodes(i),:));
% %         % Only apply if it is a true dead end, no flux with multiple
% %         % channels is not affected
% %         if length(col)==1
% %             rr=(D.*dt)./((2*channelLength.^2));
% %             A(boundaryNodes(i),col)=-rr;
% %             A(boundaryNodes(i),boundaryNodes(i))=1+rr;
% %         end
%     end
% end
% 
%     

end

