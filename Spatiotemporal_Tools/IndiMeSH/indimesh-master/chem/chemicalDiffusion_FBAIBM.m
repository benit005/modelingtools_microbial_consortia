function [ handles ] = chemicalDiffusion_FBAIBM( handles)
%[ handles ] = chemicalDiffusion_FBAIBM( handles )
% Calculates bacterial consumption (reaction term) andchemical diffusion
%
%
%INPUT
% handles       FBA-IB model struct
%
%OPTIONAL INPUT
% []            No optional input required
%
%OUTPUTS
% handles       FBA-IB model struct
%
% Benedict Borer 10/2016
% BB, 10/2016, Created file
% BB, 03/2018, Changed to diffusion using sub time stepping

iteration=handles.temp.iteration;
C_matWater=handles.chem.concWater;

% Bacterial consumption
C_matWater=C_matWater-handles.chem.consumption;
C_matWater(C_matWater<0)=0;

n=handles.temp.iteration;
if n~=1 && handles.hydro.psi(n)~=handles.hydro.psi(n-1)
    [ handles ] = updateChemicalDomain_FBAIBM( handles, iteration );
end

% Setting of aqueous boundary conditions
for ss=1:length(handles.geom.boundaryGroups)
    boundaryNodeID=handles.geom.boundaryGroups{ss};
    boundaryConc=handles.chem.Cb{ss}(iteration,:);
    substrateID=boundaryConc>=0;
    C_matWater(boundaryNodeID,substrateID)=repmat(boundaryConc(substrateID),length(boundaryNodeID),1);
end

% before=sum(C_matWater.*repmat(handles.hydro.volumeWater,1,4));
% Solve diffusion
for cc=1:handles.chem.nsubstrates
    if handles.chem.DWater(cc)==0
        C_matWater(:,cc)=handles.chem.C0(:,cc);
    else
        for kk=1:handles.temp.NtSub
            C_matWater(:,cc) = diffuseSubstrate_FBAIBM( C_matWater(:,cc),handles.chem.laplacian{cc});
        end
    end
    % Add an observation of total chemical mass
    handles.obs.totalNutrients(handles.temp.iteration,:)=sum(C_matWater.*repmat(handles.hydro.volumeWater,1,handles.chem.nsubstrates));
end


% after=sum(C_matWater.*repmat(handles.hydro.volumeWater,1,4));
% ratio=before./after

C_matWater(C_matWater<0)=0;



handles.chem.concWater=C_matWater;
end

