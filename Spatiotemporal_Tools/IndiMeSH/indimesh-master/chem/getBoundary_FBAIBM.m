function [ nodeID ] = getBoundary_FBAIBM( boundaryGroups,sourceID )
%[ nodeID ] = getSource_FBAIBM( handles,substrateID )
% Obtainthe node ID's of source nodes for a specific substrate
%
%
%INPUT
% boundaryGroups    cell with all nodes of one boundary
% sourceID          integer containing the boundary group
%
%OPTIONAL INPUT
% []                No optional input required  
%                
%OUTPUTS
% nodeID            vector with node ID's of source locations for the substrate  
%
%
%
% Benedict Borer 10/2016
% BB, 10/2016, Created file

if sourceID==0
    nodeID=[];
else
    nodeID=boundaryGroups{sourceID};
end


end

