function [ Cvector ] = diffuseSubstrate_FBAIBM( Cvector,laplacian )
% Diffuses the substrate using a forward in time, central in space FTCS
% scheme. Boundary conditions are set using ghost nodes
%
%
%INPUT
% Cvector           Vector containing concentrations
% laplacian         Laplacian matrix for diffusion
%
%OPTIONAL INPUT
% []            No optional input required  
%                
%OUTPUTS
% Cvector       Updated concentration vector
%
% Benedict Borer 10/2016
% BB, 10/2016, Created file

Cvector(Cvector<0)=0;
Cvector=laplacian\Cvector;
end

