function [ Cmat ] = setChemicalConcentrations_FBAIBM( Cmat,nodes,concentration, substrate )
% Obtainthe node ID's of source nodes for a specific substrate
%
%
%INPUT
% handles           FBA-IB model struct      
% substrateID       ID of the substrate
%
%OPTIONAL INPUT
% []                No optional input required  
%                
%OUTPUTS
% nodeID            vector with node ID's of source locations for the substrate   

          

% Benedict Borer 10/2016
% BB, 10/2016, Created file



[numNodes,numSubs]=size(Cmat);


if substrate>numSubs
    display('Invalid substrate selected!');
end

if nodes==0 & length(concentration)==1
    % Set the concentration at all nodes (e.g. initial concentration)
    Cmat(:,substrate)=concentration;
elseif nodes==0 & length(concentration)==numNodes
    Cmat(:,substrate)=concentration;
elseif length(nodes)==length(concentration)
    % Set the exact concentration at all nodes (e.g. start from steady state)
    Cmat(nodes,substrate)=concentration;
elseif length(concentration)==1 & length(nodes)>=1
    % Set the concentration at boundary nodes
    Cmat(nodes,substrate)=concentration;
else
    display('Input has wrong format!')
end

end

