function [ handles ] = prepareModelPaths_FBAIBM( modelPath, filePath, metabolicPath, saveStump, hydrationID, inoculationID, SimulationName, ParalellOption, SolverName, videoOption)
% Creates all paths (for loading files, saving location, figure path)
%
%
%INPUT
% modelPath         String containing the FBAIBM master folder
% filePath          String containing the parameter files     
% metabolicPath     String containing the metabolic networks (mat files) 
% saveStump         String containing the name for save mat files
% hydrationID       Integer of the hydration file (which realisation)
% inoculationID     Integer of the inoculation file (which realisation)
% SimulationName    String containing the name of the simulation
% ParalellOption    Option to paralellise, either simulation, none or
%                   growth
% SolverName        Name of the solver to be used for FBA/TFA
% videoOption       Option to save video data, if 1 then saved

%
%OPTIONAL INPUT
%[]             No input required
%                
%OUTPUTS
% handles       FBA-IB model struct   
%
% Benedict Borer 10/2016
% BB, 10/2016, Created file
% BB, 04/2017, Removed the loading of geometry from separate folder
% BB, 01/2018, Added the paralell option
% BB, 11/2018, Added video option  

% Prepare basic model struct and add the logbook
[ handles] = prepareModelStruct_FBAIBM();
handles.adm.logbook=createLogBook_FBAIBM(SimulationName);


% Set the simulation parameters
handles.adm.saveStump=saveStump;
handles.adm.hydrationID=hydrationID;
handles.adm.inoculationID=inoculationID;
handles.adm.simulationName=SimulationName;
handles.adm.paralellOption=ParalellOption;
handles.adm.solverName=SolverName;
handles.adm.videoOption=videoOption;

% Set the paths in the model struct
handles.paths.modelPath=modelPath;
handles.paths.metabolicPath=metabolicPath;
handles.paths.filePath=filePath;

% Check for directory existance
checkDirectories_FBAIBM( handles );



% Create and check directory existance of output paths
saveName=[handles.adm.simulationName '_Hydration' num2str(handles.adm.hydrationID) '_Inoculation' num2str(handles.adm.inoculationID) ];
handles.paths.outputPath=fullfile(filePath,'Output',saveName);
handles.paths.figurePath=fullfile(filePath,'Figures');

if isdir(handles.paths.outputPath)==0
   mkdir(handles.paths.outputPath); 
end

if isdir(handles.paths.figurePath)==0
   mkdir(filePath,'Figures'); 
end

end

