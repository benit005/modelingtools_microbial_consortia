function [ handles ] = prepareModelStruct_FBAIBM()
%[ handles ] = prepareModelStruct_FBAIBM()
% Returns the main FBA-IB model struct called handles
%
%
%INPUT
% []            No input required                
%
%OPTIONAL INPUT
% []            No input required    
%                
%OUTPUTS
% handles       struct containing the main domains (temporal, 
%               physical, chemical, biological, administrative, 
%               observation, video observation and paths)
%
% Benedict Borer 10/2016
% BB, 10/2016, Created file


handles=struct;
handles.paths=struct;
handles.adm=struct;
handles.temp=struct;
handles.geom=struct;
handles.hydro=struct;
handles.chem=struct;
handles.bio=struct;
handles.obs=struct;
handles.video=struct;

end

