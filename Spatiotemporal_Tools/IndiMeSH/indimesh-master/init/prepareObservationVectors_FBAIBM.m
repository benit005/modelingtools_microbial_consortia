function [ handles ] = prepareObservationVectors_FBAIBM( handles )
% Prepare all the observation data structures to store data during the
% simulation
%
%
%INPUT
% handles       FBA-IB model struct                  
%
%OPTIONAL INPUT
% []            No optional input required  
%                
%OUTPUTS
% handles       FBA-IB model struct  
%
% Benedict Borer 10/2016
% BB, 10/2016, Created file


% Create data structures for continuous storing (i.e. every time step)
handles.obs.totalCells=zeros(handles.temp.Nt,1);
handles.obs.totalMass=zeros(handles.temp.Nt,1);
handles.obs.totalMassgrowth=zeros(handles.temp.Nt,1);

handles.obs.speciesConsumption=cell(handles.bio.nspecies,1);
for kk=1:handles.bio.nspecies
    handles.obs.speciesConsumption{kk}=zeros(handles.temp.Nt,handles.chem.nsubstrates);
    handles.obs.totalCells(1)=handles.obs.totalCells(1)+length(handles.bio.species{kk}.x);
end
handles.obs.totalNutrients=zeros(handles.temp.Nt,handles.chem.nsubstrates);
handles.obs.specieCells=zeros(handles.temp.Nt,2);
handles.obs.doublingHistory=[];
handles.obs.speciesHistory=[];
handles.obs.consumHistory=[];
handles.obs.doublingIteration=[];
handles.obs.doublingLocation=[];
handles.obs.doublingX=[];
handles.obs.doublingY=[];
handles.obs.birthLocation=[];
handles.obs.generation=[];
handles.obs.deaths=0;
handles.obs.births=0;
handles.obs.totalDist=[];


% % Prepare all the data structures for the video data
% handles.video.speciesDistribution=cell(handles.bio.nspecies,1);
% for kk=1:handles.bio.nspecies
%     handles.video.speciesDistribution{kk}=zeros(handles.geom.npts,handles.temp.NtVideo);
% end
% 
% 
% handles.video.chemicalDistribution=cell(handles.chem.nsubstrates,1);
% for kk=1:handles.chem.nsubstrates
%     handles.video.chemicalDistribution{kk}=zeros(handles.geom.npts,handles.temp.NtVideo);
% end

if handles.adm.videoOption~=0
    % Prepare all the data structures for the video data
    nbac=handles.obs.totalCells(1);
    handles.video.inocLoc=zeros(nbac,handles.temp.NtVideo);
    handles.video.inocC=zeros(nbac,handles.temp.NtVideo);
    handles.video.inocO=zeros(nbac,handles.temp.NtVideo);
    handles.video.inocDensity=zeros(nbac,handles.temp.NtVideo);
    handles.video.weight=zeros(nbac,handles.temp.NtVideo);
    handles.video.grate=zeros(nbac,handles.temp.NtVideo);
end



end

