function [ nodeDimension, nsubstrates, nspecies, ncorners ] = loadSimulationSpec_FBAIBM( handles )
% Loads the basic simulation parameters which will be used to check input
% of the other files.
%
%
%INPUT
% handles           FBA-IB model struct
%
%OPTIONAL INPUT
% []                No optional input required
%
%OUTPUTS
% nodeDimension     Dimension of the nodes, smaller or equal to space
% nsubstrates      	Number of substrates simulated
% nspecies          Number of bacterial species
% ncorners          Number of corners of the pore (specifies geometry)
%
% Benedict Borer 10/2016
% BB, 10/2016, Created file
% BB, 03/2017, Added space dimension and node dimension



% Create the data structure
paths=handles.paths;

% get the path and load parameters
simulationFile=fullfile(paths.filePath,'IO','SimulationSpec.txt');
% Check the existance of the file, throw error if absent
checkFileExistance_FBAIBM(handles.adm.logbook,simulationFile, mfilename());



[ StringColumns, NumericColumns ] = readTxtFile_FBAIBM(simulationFile, ',', 2,4);
nodeDimension = cell2mat(NumericColumns(:, 1)); % 1 (pore) or 2 (patch)
ncorners = cell2mat(NumericColumns(:, 2)); % integer number, depends on node dimension
nsubstrates = cell2mat(NumericColumns(:, 3)); % any integer number
nspecies = cell2mat(NumericColumns(:, 4)); % any integer number

% Check dimension of nodes, greater than 0, smaller than 4, integer and
% smaller than space dimension
if nodeDimension < 1 || nodeDimension >2
    errorString=[simulationFile ' specified the dimension of nodes to an impossible number'];
    addLogBookEntry_FBAIBM( handles.adm.logbook, errorString, mfilename())
    error('Node dimension impossible: Check logbook entry')
elseif floor(nodeDimension)~=nodeDimension
    errorString=[simulationFile ' specified the dimension of nodes as non integer'];
    addLogBookEntry_FBAIBM( handles.adm.logbook, errorString, mfilename())
    error('Node dimension impossible: Check logbook entry')
end

% check that the number of substrates is not smaller than one
if nsubstrates<1
    errorString=[simulationFile ' specified the number of substrates to be 0'];
    addLogBookEntry_FBAIBM( handles.adm.logbook, errorString, mfilename())
    error('Number of substrates defined as 0: Check logbook entry')
elseif floor(nsubstrates)~=nsubstrates
    errorString=[simulationFile ' specified the number of substrates as non integer'];
    addLogBookEntry_FBAIBM( handles.adm.logbook, errorString, mfilename())
    error('Number of substrates impossible: Check logbook entry')
end

% Check that the number of species is not smaller than one
if nspecies<1
    errorString=[simulationFile ' specified the number of species to be 0'];
    addLogBookEntry_FBAIBM( handles.adm.logbook, errorString, mfilename())
    error('Number of species defined as 0: Check logbook entry')
elseif floor(nspecies)~=nspecies
    errorString=[simulationFile ' specified the number of species as non integer'];
    addLogBookEntry_FBAIBM( handles.adm.logbook, errorString, mfilename())
    error('Number of species impossible: Check logbook entry')
end

% No corners smaller than 3 allowed (no geometrical sense)
if ncorners<3 || ncorners > 6
    errorString=[simulationFile ' specified the number of corners wrong'];
    addLogBookEntry_FBAIBM( handles.adm.logbook, errorString, mfilename())
    error('Number of corners < 3 or > 6: Check logbook entry')
elseif floor(ncorners)~=ncorners
    errorString=[simulationFile ' specified the number of species as non integer'];
    addLogBookEntry_FBAIBM( handles.adm.logbook, errorString, mfilename())
    error('Number of species impossible: Check logbook entry')
end


% Check the combination of spece dimension, node dimension and given
% corners
if nodeDimension == 1 && ismember(ncorners,[3,4])==0
    errorString=['In ' simulationFile ': combination of node dimension ' nodeDimension 'with number of corners ' ncorners 'is not possible.'];
    addLogBookEntry_FBAIBM( handles.adm.logbook, errorString, mfilename())
    error('Space dimension, node dimension and number of corners missmatch: Check logbook entry')
elseif nodeDimension == 2 && ismember(ncorners,[4,6])==0
    errorString=['In ' simulationFile ': combination of node dimension ' nodeDimension 'with number of corners ' ncorners 'is not possible.'];
    addLogBookEntry_FBAIBM( handles.adm.logbook, errorString, mfilename())
    error('Space dimension, node dimension and number of corners missmatch: Check logbook entry')
end


end

