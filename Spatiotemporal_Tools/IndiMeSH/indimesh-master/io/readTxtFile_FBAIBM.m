function [ rawStringColumns, rawNumericColumns ] = readTxtFile_FBAIBM( filename, delimiter, startRow, totColumns, varargin )
% Function to read the basic text files created by the GUI or to realise
% simulation replicated
%
%
%INPUT
% filename              Name of the file to be read
% delimiter             Delimiter used for the textscan function
% startRow              Start row (depending if the file has headers)
% totColumns            Total number of columns
% 
%
%OPTIONAL INPUT
% varargin              Identifier for columns containing string input
%                
%OUTPUTS
% rawStringColumns      Output of strings
% rawNumericColumns     Output of numeric values
%
% Benedict Borer 10/2016
% BB, 10/2016, Created file
% BB, 03/2017, Improved documentation, narginchk



% Check number of input arguments
narginchk(4, 5);
columnsVector=1:totColumns;
if nargin==5
   stringColumns=varargin{1}; 
   numericColumns=columnsVector(~ismember(columnsVector,stringColumns));
else
   stringColumns=[];
   numericColumns=1:totColumns;
end
% Create formatSpec
formatSpec = [repmat('%s',1,totColumns) '%[\n\r]'];


% open the file and read data
fileID = fopen(filename,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'HeaderLines' ,startRow-1, 'ReturnOnError', false);
fclose(fileID);



raw = repmat({''},length(dataArray{1}),length(dataArray)-1);
for col=1:length(dataArray)-1
    raw(1:length(dataArray{col}),col) = dataArray{col};
end
numericData = NaN(size(dataArray{1},1),size(dataArray,2));


for col=numericColumns
    rawData = dataArray{col};
    for row=1:size(rawData, 1);
        regexstr = '(?<prefix>.*?)(?<numbers>([-]*(\d+[\,]*)+[\.]{0,1}\d*[eEdD]{0,1}[-+]*\d*[i]{0,1})|([-]*(\d+[\,]*)*[\.]{1,1}\d+[eEdD]{0,1}[-+]*\d*[i]{0,1}))(?<suffix>.*)';
        try
            result = regexp(rawData{row}, regexstr, 'names');
            numbers = result.numbers;
            
            % Detected commas in non-thousand locations.
            invalidThousandsSeparator = false;
            if any(numbers==',');
                thousandsRegExp = '^\d+?(\,\d{3})*\.{0,1}\d*$';
                if isempty(regexp(thousandsRegExp, ',', 'once'));
                    numbers = NaN;
                    invalidThousandsSeparator = true;
                end
            end
            % Convert numeric strings to numbers.
            if ~invalidThousandsSeparator;
                numbers = textscan(strrep(numbers, ',', ''), '%f');
                numericData(row, col) = numbers{1};
                raw{row, col} = numbers{1};
            end
        catch me
            % no exceptions are handled directly
        end
    end
end

rawStringColumns = raw(:, stringColumns);
rawNumericColumns = raw(:, numericColumns);
end

