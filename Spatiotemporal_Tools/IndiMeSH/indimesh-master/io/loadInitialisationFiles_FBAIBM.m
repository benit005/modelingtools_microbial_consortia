function [ handles ] = loadInitialisationFiles_FBAIBM( handles )
% Initialises and loads all parameters/coefficients into the main domains
% and makes sure that the model is ready to run
%
%
%INPUT
% handles       FBA-IB model struct                  
%
%OPTIONAL INPUT
% []            No optional input required  
%                
%OUTPUTS
% handles       FBA-IB model struct  
%
% Benedict Borer 10/2016
% BB, 10/2016, Created file


% Load basic model parameters
[ nodeDimension, nsubstrates, nspecies, ncorners] = loadSimulationSpec_FBAIBM( handles );

% Load temporal domain
handles.temp=loadTemporalDomain_FBAIBM(handles);

% Load physical domain
handles.geom=loadPhysicalDomain_FBAIBM(handles, nodeDimension, ncorners);

% Load physical domain
handles.hydro=loadHydrologicalDomain_FBAIBM( handles);

% Load chemical domain
handles.chem=loadChemicalDomain_FBAIBM(handles, nsubstrates);

% Load biological domain
handles.bio=loadBiologicalDomain_FBAIBM(handles, nspecies);

% Update required model parameters
[handles]=updateChemicalDomain_FBAIBM(handles);

% Apply initial chemical concentrations
for jj=1:handles.chem.nsubstrates
    handles.chem.concWater=setChemicalConcentrations_FBAIBM(handles.chem.concWater,0,handles.chem.C0(:,jj),jj);
end

% Prepare observational vectors
[ handles ] = prepareObservationVectors_FBAIBM( handles );
end

