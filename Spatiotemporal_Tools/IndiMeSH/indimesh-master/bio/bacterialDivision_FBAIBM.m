function [ handles ] = bacterialDivision_FBAIBM( handles )
% Calculates bacterial divisiopn. Bacteria divide if reaching a specific
% mass.
%
%
%INPUT
% handles       FBA-IB model struct
%
%OPTIONAL INPUT
% []            No optional input required
%
%OUTPUTS
% handles       FBA-IB model struct
%
% Benedict Borer 10/2016
% BB, 10/2016, Created file


for sn=1:handles.bio.nspecies
    bac=handles.bio.species{sn};
    % store the old number of cells (before division)
    doubling_index=bac.m>bac.mmax;
    % increase number of cells with one
    new_bacteria=sum(doubling_index);
    
    if new_bacteria > 0
        bac.n = bac.n + new_bacteria;
        handles.obs.births=handles.obs.births+new_bacteria;
        % coordinates, species and radius of the new cell (n)
        bac.x  = [bac.x; bac.x(doubling_index)];
        bac.y  = [bac.y; bac.y(doubling_index)];
        bac.z  = [bac.z; bac.z(doubling_index)];
        bac.pos = [bac.pos; bac.pos(doubling_index)];
        bac.posOld = [bac.posOld; bac.posOld(doubling_index)];
        bac.cpos = [bac.cpos; bac.cpos(doubling_index)];
        bac.progress = [bac.progress; bac.progress(doubling_index)];
        bac.channel = [bac.channel; bac.channel(doubling_index)];
        bac.ancestor = [bac.ancestor ; bac.ancestor(doubling_index)];
        bac.tumble = [bac.tumble; bac.tumble(doubling_index)];
        bac.motility.v = [bac.motility.v; bac.motility.v(doubling_index)];
        bac.oppositePos=[bac.oppositePos; bac.oppositePos(doubling_index)];
        
        
        % Store consumption history and reset
        handles.obs.speciesHistory=[handles.obs.speciesHistory; repmat(sn,new_bacteria,1)];
        handles.obs.consumHistory=[handles.obs.consumHistory; bac.consum(doubling_index,:)];
        bac.consum(doubling_index,:)=0;
        bac.consum = [bac.consum; bac.consum(doubling_index,:)];
        
        
        %Calculate the splitting of mass and growth rate
        fraction = 0.45 + 0.1*rand(new_bacteria,1);
        bac_mnew = (1-fraction).*bac.m(doubling_index);
        bac.m(doubling_index) = bac.m(doubling_index).*fraction;
        bac.m = [bac.m; bac_mnew];
        bac.mu = [bac.mu; bac.mu(doubling_index,:)];
        bac.muPercentage = [bac.muPercentage; bac.muPercentage(doubling_index,:)];
        bac.muOld = [bac.muOld; bac.muOld(doubling_index)];
        
        % Store the doubling history and reset age
        handles.obs.doublingHistory=[handles.obs.doublingHistory; bac.age(doubling_index)];
        handles.obs.doublingIteration=[handles.obs.doublingIteration; ones(new_bacteria,1).*handles.temp.t];
        handles.obs.doublingX=[handles.obs.doublingX; bac.x(doubling_index)];
        handles.obs.doublingY=[handles.obs.doublingY; bac.y(doubling_index)];
        handles.obs.totalDist=[handles.obs.totalDist; bac.totalDist(doubling_index)];
        handles.obs.doublingLocation=[handles.obs.doublingLocation; bac.pos(doubling_index)];
        bac.totalDist = [bac.totalDist; bac.totalDist(doubling_index)];
        bac.totalDist(doubling_index)=0;
        
        handles.obs.birthLocation=[handles.obs.birthLocation; bac.birthPlace(doubling_index)];
        handles.obs.generation=[handles.obs.generation; bac.generation(doubling_index)];
        bac.generation(doubling_index)=bac.generation(doubling_index)+1;
        bac.generation = [bac.generation; bac.generation(doubling_index)];
        bac.age = [bac.age; zeros(new_bacteria,1)];
        bac.age(doubling_index)=0;
        
        bac.birthPlace=[bac.birthPlace; bac.pos(doubling_index)];
        bac.birthPlace(doubling_index) = bac.pos(doubling_index);
        bac.angleHorz=[bac.angleHorz; bac.angleHorz(doubling_index)];
        bac.angleVert=[bac.angleVert; bac.angleVert(doubling_index)];
        
    end
    handles.bio.species{sn}=bac;
end


end

