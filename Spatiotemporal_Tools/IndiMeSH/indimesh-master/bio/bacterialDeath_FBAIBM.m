function [ handles ] = bacterialDeath_FBAIBM( handles )
% Calculates bacterial death. Cells die if falling below a critical value
%
%
%INPUT
% handles       FBA-IB model struct                  
%
%OPTIONAL INPUT
% []            No optional input required  
%                
%OUTPUTS
% handles       FBA-IB model struct 
%
% Benedict Borer 10/2016
% BB, 10/2016, Created file

for sn=1:handles.bio.nspecies
    bac=handles.bio.species{sn};
    delete_index=bac.m<bac.mcrit;
    bacteria_death=sum(delete_index);
    if bacteria_death>0
        handles.obs.deaths=handles.obs.deaths+bacteria_death;
        
        bac.x(delete_index)  = [];
        bac.y(delete_index)  = [];
        bac.z(delete_index)  = [];
        bac.pos(delete_index)= [];
        bac.posOld(delete_index)= [];
        bac.cpos(delete_index)= [];
        bac.progress(delete_index)  = [];
        bac.m(delete_index)  = [];
        bac.ancestor(delete_index) = [];
        bac.oppositePos(delete_index)= [];
        bac.consum(delete_index,:)= [];
        bac.birthPlace(delete_index,:)= [];
        bac.generation(delete_index)=[];
        bac.totalDist(delete_index)=[];
        bac.mu(delete_index,:)=[];
        bac.muPercentage(delete_index,:)=[];
        bac.muOld(delete_index,:)=[];
        bac.age(delete_index)=[];
        bac.tumble(delete_index)=[];
        bac.motility.v(delete_index)=[];
        bac.angleHorz(delete_index)=[];
        bac.angleVert(delete_index)=[];
        bac.channel(delete_index)=[];
        bac.n=bac.n-bacteria_death;
    end
    handles.bio.species{sn}=bac;
end

sn=1;
while sn<=handles.bio.nspecies
    if handles.bio.species{sn}.n<=0
        handles.bio.deceased{sn}=handles.bio.species{sn};
        handles.bio.nspecies=handles.bio.nspecies-1;
        handles.bio.species(sn)=[];
    end
    sn=sn+1;
end


end

