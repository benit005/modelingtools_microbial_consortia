function [ output_args ] = preSimulationAnalysis_FBAIBM( handles )
%   Detailed explanation goes here


colorScheme='rgbkcm';

%%% Physical analysis

%%% Chemical analysis

%%% Biological analysis
for sn=1:handles.bio.nspecies
    bac=handles.bio.species{sn};
    [ T, concentrationMat, grate ] = metNetworkAnalysis_FBAIBM( bac,substrateNames );
    filename=[handles.paths.outputPath handles.adm.sep bac.genus '_' bac.name '_MetabolicAnalysis.txt'];
    writetable(T,filename)
    
    for kk=1:handles.chem.nsubstrates
    figure;
    plot(concentrationMat(:,kk),grate,'ok','Markerfacecolor',colorScheme(kk))
    title([bac.genus ' ' bac.name])
    xlabel(substrateNames(kk))
    ylabel('Growth rate [s^{-1}]')
    end
end





end



