function cplex = changeToCPLEX(model,changeToInt)
% takes a tFBA model and changes it into a the MATLAB cplex class

[num_constr,num_vars]=size(model.A);

if nargin == 1
    changeToInt = false;
end

% convert vtypes and contypes into the right format
vtypes = '';
for i=1:num_vars
    if changeToInt
        if (strcmp(model.vartypes{i,1},'C'))
            var_type='I';
        else
            var_type=model.vartypes{i,1};
        end
    else
        var_type=model.vartypes{i,1};
    end
   vtypes = strcat(vtypes,var_type);
end

for i=1:num_constr
    if strcmp(model.constraintType{i,1},'=')
        lhs(i,1) = model.rhs(i);
        rhs(i,1) = model.rhs(i);
    elseif strcmp(model.constraintType{i,1},'>')
        lhs(i,1) = model.rhs(i);
        rhs(i,1) = inf;
    elseif strcmp(model.constraintType{i,1},'<')
        rhs(i,1) = model.rhs(i);
        lhs(i,1) = -Inf;
    else
        error('constraint type not recognised.');
    end
end

% formulating the cplex model
% Use arrays to populate the model
cplex = Cplex(model.description);

if (model.objtype == -1)    
    cplex.Model.sense = 'maximize';
else
    cplex.Model.sense = 'minimize';
end

cplex.Model.A     = model.A;
cplex.Model.obj   = model.f;
cplex.Model.lb    = model.var_lb;
cplex.Model.ub    = model.var_ub;
cplex.Model.ctype = vtypes;
cplex.Model.rhs = rhs;
cplex.Model.lhs = lhs;

cplex.Model.colname = char(model.varNames);
cplex.Model.rowname = char(model.constraintNames);
cplex.Param.read.scale.Cur=-1;
% Set the tolerance
cplex.Param.mip.tolerances.integrality.Cur = 1E-9;
% turn off the log of nodes
cplex.Param.output.clonelog.Cur = 0;
