function [sol,cplex] = solveTFBAmodel(tModel,changeToInt,solver,printall)
% this function solves a TFBA problem using either Gurobi (through
% Gurobi_mex) or CPLEX
 
if nargin < 2
    changeToInt = false;
end
 
if nargin < 3
    solver = 'cplex';
end
 
if nargin < 4
    printall = false;
end
 
num_constr = length(tModel.constraintType);
num_vars = length(tModel.vartypes);
 
contypes = '';
vtypes = '';
 
if (strcmp(solver,'gurobi'))
    % convert contypes and vtypes into the right format
    for i=1:num_constr
       contypes = strcat(contypes,tModel.constraintType{i,1});
    end
 
    for i=1:num_vars
       vtypes = strcat(vtypes,tModel.vartypes{i,1});
    end
 
    if nargin < 4
        clear opts
        %opts.IterationLimit = 10000;
        opts.FeasibilityTol = 1e-9;
        opts.IntFeasTol = 1e-9;
        opts.OptimalityTol = 1e-9;
        % opts.Method = 1; % 0 - primal, 1 - dual
        opts.Presolve = -1; % -1 - auto, 0 - no, 1 - conserv, 2 - aggressive
        opts.Display = 1;
        opts.DisplayInterval = 1;
        opts.OutputFlag = 0;
        %opts.LogFile = [tModel.description '.log'];
        opts.WriteToFile = [tModel.description '.lp'];
    end
 
    [x,val,exitflag,output] = gurobi_mex(tModel.f,tModel.objtype,sparse(tModel.A),tModel.rhs,contypes,tModel.var_lb,tModel.var_ub,vtypes,opts);
 
    if (abs(val) < opts.IntFeasTol)
        val = 0;
    end
elseif (strcmp(solver,'cplex'))
    
    % Optimize the problem fifitta26686
    cplex = changeToCPLEX(tModel,changeToInt);
    cplex.Param.timelimit.Cur=500;
%    cplex.Param.simplex.tolerances.feasibility.Cur=10^-6;
    cplex.Solution = [];
    ans = cplex.solve();
 
    if isfield(cplex.Solution,'x')
        if (abs(ans.bestobjval) < 1E9)
            x = cplex.Solution.x;
            x(find(abs(x) < 1E-9))=0;
        end
    else
        x=[];
    end
    
elseif (strcmp(solver,'glpk'))
    
    csense='';
    % Set up problem
    for i=1:length(tModel.constraintType)
        if tModel.constraintType{i} == '='
            csense(i)='S';
        elseif tModel.constraintType{i} == '<'
            csense(i)='U';
        elseif tModel.constraintType{i} == '>'
            csense(i)='L';
        end
    end
    
    %whos csense vartype
    csense = columnVector(csense);
    vartype = columnVector(char(tModel.vartypes));
    %whos csense vartype
    
    % Solve problem
    [x,f,stat,extra] = glpk(tModel.f,tModel.A,tModel.rhs,tModel.var_lb,tModel.var_ub,csense,vartype,tModel.objtype);
    
    % Handle solution status reports
    if (stat == 5)
        solStat = 1; % optimal
        sol.x=x;
        sol.val=f;
        sol.stat=stat;
    elseif (stat == 6)
        solStat = 2; % unbounded
    elseif (stat == 4) 
        solStat = 0; % infeasible
 
    elseif (stat == 171)
        solStat = 1; % Opt integer within tolerance
        sol.x=x;
        sol.val=f;
        sol.stat=stat;
    elseif (stat == 173)
        solStat = 0; % Integer infeas
    elseif (stat == 184)
        solStat = 2; % Unbounded
    elseif (stat == 172)
        solStat = 3; % Other problem, but integer solution exists
    else
        solStat = -1; % No integer solution exists
    end
    
    
    %% older version using cplexmilp
%     % first we have to reorder the constraints into equality and inequality
%     
%     eq_cons_row_indices = find(ismember(tModel.constraintType,'='));
%     ineq_cons_row_indices = find(ismember(tModel.constraintType,'<'));
%     
%     Aineq = tModel.A(ineq_cons_row_indices,:);
%     bineq = tModel.rhs(ineq_cons_row_indices);
%     
%     Aeq = tModel.A(eq_cons_row_indices,:);
%     beq = tModel.rhs(eq_cons_row_indices);
%     
%     for i=1:num_vars
%        vtypes = strcat(vtypes,tModel.vartypes{i,1});
%     end
%     
%     options = cplexoptimset;
%     options.Diagnostics = 'on';
%     options.TolXInteger = 1e-9;
%     options.TolFun = 1e-9;
%     options.TolRLPFun = 1e-9;
% %     options.Simplex = 'on';
% %     options.ExportModel = 'test.lp';
% %     options.mip.strategy.search=1;
%     
%     [x, val, exitflag, output] = cplexmilp (tModel.f, Aineq, bineq, Aeq, beq,...
%                                 [ ], [ ], [ ], tModel.var_lb, tModel.var_ub, vtypes, [ ], options);
    
    
else
   error('solver not recognised. Only gurobi, glpk and cplex allowed.'); 
end
% Gurobi gives no lambda (Pi, or Lagrange multipliers) for MIPs, without calling modelfix
 
if ~isempty(x)
    
    if (strcmp(solver,'gurobi'))
        sol.x = x;
        sol.val = val;
        sol.exitflag = exitflag;
        sol.output = output;
    elseif (strcmp(solver,'cplex'))
        sol.x = cplex.Solution.x;
        sol.val = cplex.Solution.objval;
        sol.x(find(abs(sol.x) < 1E-7))=0;
        
        if (cplex.Solution.status == 101) || (cplex.Solution.status == 102) % MIP optimal or MIP optimal tol
            sol.exitflag = 2;
        else
            sol.exitflag = 0;
        end
    end
 
    if isfield(tModel,'types')
        prefixList = tModel.types.prefix;
        typesList = tModel.types.type;
    end
    
    % disp('Solution:');disp(x')
    disp('Optimal obj value:');disp(sol.val);
    %disp('Exit flag:');disp(exitflag)
    %disp('Optimization info:');disp(output);
 
    if (printall) && ~isempty(x)
 
       printLPformat(tModel);
 
       EXCELfilename = [tModel.description '_sol.xls'];
       SheetData{1} = 'variables.txt';
       SheetData{2} = 'constraints.txt';
 
       writeToEXCEL(EXCELfilename,SheetData);
 
       for i=1:length(SheetData)
           command = ['rm ' SheetData{i}];
           system(command);   
       end
    end
else
    sol.x = [];
    sol.val = 0;
    sol.exitflag = 0;
    disp('no solution');
end
