function [ volume ] = calculateVolumeChannel_FBAIBM(area,channellength)
% Calculates the volume of all channels within the network depending on
% cross sectional area
%
%INPUT
% area              cross sectional area used volume calculation
% channellength     side length of the polygon
%
%OPTIONAL INPUT
% []            No optional input required  
%                
%OUTPUTS
% volume        Volume of the channels  
%
% Benedict Borer 10/2016
% BB, 10/2016, Created file
% BB, 05/2017, Changed to volume calculation based on matric potential
% BB, 10/2017, Changed to volume calculation based on channels (not pores)

volume=area.*channellength;


end

