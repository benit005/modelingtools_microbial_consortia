function [ volume ] = calculateVolumePore_FBAIBM(channelVolume,adj,interrelation)
% Calculates the volume of all channels within the network depending on
% cross sectional area
%
%INPUT
% channelVolume     volume of each individual channel
% adj               network adjacency matrix
% interrelation     vector relating find(adj) to find(triu(adj))
%
%OPTIONAL INPUT
% []            No optional input required  
%                
%OUTPUTS
% volume        Volume of the pores  
%
% Benedict Borer 10/2017
% BB, 10/2017, Created file

[npts,~]=size(adj);
[r,c,~]=find(adj);
% volume=full(sum(sparse(r,c,channelVolume(interrelation),npts,npts),2));
volume=full(sum(sparse(r,c,channelVolume(interrelation),npts,npts),2))./2;
end

