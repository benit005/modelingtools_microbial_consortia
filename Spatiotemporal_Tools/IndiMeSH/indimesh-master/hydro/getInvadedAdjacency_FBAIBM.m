function [ airAdj ] = getInvadedAdjacency_FBAIBM(saturation,adj, convertAdj)
% Produces the adjacency matrix of air filled pores. This is required to
% subsequently perform invasion percolation calculation (gaseous sources
% determination)
%
%INPUT
% saturation        degree of saturation of nodes
% adj               adjacency matrix of nodes
% convertAdj        ID for creating sparse matrix from links data
%
%OPTIONAL INPUT
% []                No optional input required
%
%OUTPUTS
% airAdj            adjacency matrix of air invaded pores
%
% Benedict Borer 05/2017
% BB, 05/2017, Created file

saturation(saturation<1)=0;
saturation=saturation*-1+1;
[r,c,v]=find(adj);
airAdj=sparse(r,c,saturation(convertAdj));
end



