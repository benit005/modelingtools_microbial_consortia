function [ interfacialRadius ] = calculateInterfacialCurvature_FBAIBM( psi )
% Calculate the interfacial curvature depending on the matric potential.
% This function assumes a water temparature of 20�C
%
%INPUT
% psi               matric potential [Pa]
%
%OPTIONAL INPUT
% []                No optional input required  
%                
%OUTPUTS
% interfacialRadius radius of interfacial curvature  
%
% Benedict Borer 05/2017
% BB, 05/2017, Created file

sigma=0.0727; % Surface tension of water at 20�C (N/m)
interfacialRadius=sigma./psi;
end

