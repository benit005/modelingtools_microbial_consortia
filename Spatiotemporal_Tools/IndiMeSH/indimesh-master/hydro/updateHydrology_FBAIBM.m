function [ handles ] = updateHydrology_FBAIBM( handles )
% Updates the hydrology of the model: water volume, water film thickness
% , diffusional area, invasion percolation
%
%
%INPUT
% handles       FBA-IB model struct
%
%OPTIONAL INPUT
% []            No optional input required
%
%OUTPUTS
% handles       FBA-IB model struct
%
% Benedict Borer 05/2017
% BB, 05/2017, Created file
geom=handles.geom;
hydro=handles.hydro;
iteration=handles.temp.iteration;
matricPotential=hydro.psi(iteration); % Matric potential in Pascal

if handles.geom.nodeDimension==1
    % [m^2] Calculate diffusional area
    if handles.geom.ncorners==3
        angles=[geom.links.centerAngle geom.links.upperAngle geom.links.upperAngle];
    elseif handles.geom.ncorners==4
        angles=[geom.links.centerAngle geom.links.upperAngle geom.links.centerAngle geom.links.upperAngle];
    end
    
    [hydro.diffAreaWater,hydro.saturation]=calculateWaterAreaPore_FBAIBM(matricPotential,hydro.totalArea,angles,geom.adj,geom.links.adj,hydro.airLocation,handles.geom.convertAdj);
    [hydro.wft]=calculateWFTPore_FBAIBM(matricPotential,geom.links.centerAngle,geom.links.upperAngle, hydro.saturation,geom.links.radius);
    [hydro.volumeChannel] = calculateVolumeChannel_FBAIBM(hydro.diffAreaWater,geom.links.length);
    [hydro.volumeWater] = calculateVolumePore_FBAIBM(hydro.volumeChannel,geom.adj,geom.convertAdj);

%     hydro.diffAreaAir=calculateAirAreaPore_FBAIBM( hydro.diffAreaWater,hydro.totalArea);
%     hydro.volumeAir=calculateVolumePore_FBAIBM(hydro.totalArea-hydro.volumeChannel,geom.adj,geom.convertAdj);
elseif handles.geom.nodeDimension==2
    error('Not yet implemented!')
end

% Add new data to handles structure
handles.geom=geom;
handles.hydro=hydro;
end

