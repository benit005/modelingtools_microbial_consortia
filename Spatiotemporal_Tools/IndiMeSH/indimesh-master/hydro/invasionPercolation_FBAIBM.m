function [ correction ] = invasionPercolation_FBAIBM(saturation,adj,cadj,airLocation, interrelation )
% Obtains a vector of which channels are calculated to be desaturated, but
% do not have a connected path to a liquid air interface
%
%INPUT
% saturation        degree of saturation of nodes
% adj               adjacency matrix of nodes
% cadj              Connectivity between nodes and corresponding channels
% airLocation       ID of nodes in contact with air
% interrelation     Relation between find(adj) and find(triu(adj))
%
%OPTIONAL INPUT
% []                No optional input required
%
%OUTPUTS
% correction        ID of nodes that are saturated due to bottle necks (i.e. need to be kept saturated)
%
% Benedict Borer 05/2017
% BB, 05/2017, Created file
% Check for air invasion into the network
[ airAdj ] = getInvadedAdjacency_FBAIBM(saturation,adj,interrelation);

% figure;
% hold on
% geom=handles.geom;
% gplot(geom.adj,[geom.pts.X geom.pts.Y],'g')
% scatter(geom.links.X,geom.links.Y,[],handles.hydro.saturation,'filled')
% gplot(airAdj,[geom.pts.X geom.pts.Y],'r')
% axis equal tight
% axis off


[nlinks,npts]=size(cadj);

degree=full(sum(airAdj))';
A=-airAdj+(spdiags(degree,0,npts,npts)+speye(size(airAdj)));


airInvasion=zeros(npts,1);
airInvasion(airLocation)=1;
dist=A\airInvasion;

% Invaded means that the concentration at that node changed
invaded=dist~=0;
[row,~,~]=find(cadj(:,invaded));

invadedChannels=zeros(nlinks,1);
invadedChannels(row)=1;

% Corrections is 1 where the saturation needs to be adapted
correction=(-1*invadedChannels+1 & -1*saturation+1);

end

