function [ wft ] = calculateWFTPore_FBAIBM(psi, angle1,angle2, saturation, radius)
% Calculates the effective water film thickness with respect to bacterial
% cell size. This is used to obtain swimming velocity and can be used to
% calculate water volume if preferred. Can take either many different
% geometries (angles) or different matric potentials
%
%
%INPUT
% psi               Matric potential in Pascal
% angle1            Angle number 1 (triangle:bottom)
% angle2            Angle number 2 (triangle:upper)
% saturation        Degree of saturation of the pore
% radius            Radius of the inscribed pore 

%
%OPTIONAL INPUT
% []                No optional input required
%
%OUTPUTS
% wft               Water film thickness at each node in m
%
% Benedict Borer 05/2015
% BB, 05/2017, Created file

interfacialCurvature=calculateInterfacialCurvature_FBAIBM(psi);
wft1=2.*interfacialCurvature.*((1-sind(angle1/2))./(1+sind(angle1/2)));
wft2=2.*interfacialCurvature.*((1-sind(angle2/2))./(1+sind(angle2/2)));
wft=[wft1 wft2];
wft(saturation==1,:)=repmat(radius(saturation==1),1,2);
wft(saturation==1,:)=200E-6;

end
