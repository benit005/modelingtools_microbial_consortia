function [ area,saturation ] = calculateWaterAreaPore_FBAIBM( psi,totalArea,angles,adj,cadj,airLocation,interrelation)
% Calculates the diffusional area available for pores and degree of
% saturation.
%
%INPUT
% psi               Matric potential in Pa
% totalArea         Total area of each pore
% angles            [npts by 3] matrix containing [central upper upper] Angle
% adj               Adjacency matrix
% cadj              Connectivity of nodes and corresponding channels
% airLocation       Nodes in contact with air
% interrelation     Relation between find(adj) and find(triu(adj))
%
%OPTIONAL INPUT
% []                No optional input required  
%                
%OUTPUTS
% area              Area of water inside pore
% saturation        Degree of saturation
%
% Benedict Borer 05/2017
% BB, 05/2017, Created file
% BB, 10/2017, Added the updating of invasion to this function from
%              external
angularityFactor=calculateAngularityFactor_FBAIBM(angles);
interfacialRadius=calculateInterfacialCurvature_FBAIBM(psi);
Aw=(interfacialRadius.^2).*angularityFactor;
area=min(Aw,totalArea);
saturation=area./totalArea;

% Update the saturation and diffusive area by taking into account
% connectivity. I.e. only channels that have a connected path of air to the
% a liquid air interface are considered as desaturated.
correction=invasionPercolation_FBAIBM(saturation,adj,cadj,airLocation,interrelation);
saturation(correction)=1;

area(correction)=totalArea(correction);




% 
% [ airAdj ] = getInvadedAdjacency_FBAIBM(saturation,adj,interrelation);
% 
% figure;
% hold on
% geom=handles.geom;
% gplot(geom.adj,[geom.pts.X geom.pts.Y],'g')
% scatter(geom.links.X,geom.links.Y,[],handles.hydro.saturation,'filled')
% gplot(airAdj,[geom.pts.X geom.pts.Y],'r')
% axis equal tight
% axis off

end


