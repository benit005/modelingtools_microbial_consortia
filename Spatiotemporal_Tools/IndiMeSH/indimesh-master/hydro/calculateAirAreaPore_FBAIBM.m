function [ area ] = calculateAirAreaPore_FBAIBM( waterArea,totalArea)
% Calculates the diffusional area available for gaseous substances in  pores and degree of
% saturation
%
%INPUT
% waterArea     area filled by water
% totalArea     Total pore area
%
%OPTIONAL INPUT
% []            No optional input required  
%                
%OUTPUTS
% area              area of air inside pore
%
% Benedict Borer 05/2017
% BB, 05/2017, Created file

area=totalArea-waterArea;
end


