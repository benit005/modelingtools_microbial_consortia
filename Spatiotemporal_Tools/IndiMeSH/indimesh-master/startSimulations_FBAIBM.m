function []=startSimulations_FBAIBM(SimulationFolder, SimulationName, HydrationNumber, InoculationNumber, ParalellOption,CoreNumbers, SolverName, pathStump, videoOption)


SimulationNumber=round(HydrationNumber.*InoculationNumber);
modelPath=cd;

% Get the paralell workers
if strncmpi(ParalellOption,'Simulation',3) || strncmpi(ParalellOption,'Growth',3)
    p=gcp('nocreate');
    if isempty(p)
        c = parcluster('local'); % build the 'local' cluster object
        nw = c.NumWorkers;       % get the number of workers
        if nw>CoreNumbers
            nw=CoreNumbers;
        else
             display(['User wanted ' num2str(CoreNumbers) ' cores, but only ' num2str(nw) ' available!'])
        end
        parpool(nw);
    end
end

% Get values for all simulations as a cross-matrix
[HydrationMatrix,InoculationMatrix]=meshgrid(1:HydrationNumber,1:InoculationNumber);
HydrationVector=reshape(HydrationMatrix,[],1);
InoculationVector=reshape(InoculationMatrix,[],1);

% Get the folders
metabolicPath=fullfile(pathStump,'MetabolicNetworks');
filePath=fullfile(modelPath, SimulationFolder);
currentTime=getDateTimeString_FBAIBM();

        
if strncmpi(ParalellOption,'Simulation',3)
    parfor kk=1:SimulationNumber
        saveStump=[currentTime '_' SimulationName '_Hydration' num2str(kk) '_Inoculation' num2str(kk) '_'];
        main_FBAIBM(modelPath, filePath, metabolicPath, saveStump, HydrationVector(kk),InoculationVector(kk), SimulationName, ParalellOption, SolverName, videoOption);
    end
else
    for kk=1:SimulationNumber
        saveStump=[currentTime '_' SimulationName '_Hydration' num2str(kk) '_Inoculation' num2str(kk) '_'];
        main_FBAIBM(modelPath, filePath, metabolicPath, saveStump, HydrationVector(kk),InoculationVector(kk), SimulationName, ParalellOption, SolverName, videoOption);
    end
end





end



