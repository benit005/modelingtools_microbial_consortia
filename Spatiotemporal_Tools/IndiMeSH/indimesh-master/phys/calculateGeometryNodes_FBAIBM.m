function [ x,y,z,gadj, gnodeOffset ] = calculateGeometryNodes_FBAIBM( X,Y,Z,adj,npts,ndim, ncorners,sideLength )
% Calculates the position and connectivity of geometry nodes used to assign
% bacteria and for plotting
%
%
%INPUT
% X             X Coordinates of nodes
% Y             Y Coordinates of nodes
% Z             Z Coordinates of nodes
% adj           Adjacency matrix of nodes
% npts          Number of nodes
% ndim          Dimensionality of nodes
% ncorners      Number of corners (specifies geometry)
% sideLength    Distance between nodes / node and cnode
%
%OPTIONAL INPUT
% []            No optional input required  
%                
%OUTPUTS
% x             X coordinates of g nodes
% y             X coordinates of g nodes
% z             X coordinates of g nodes
% gadj          Adjacency matrix of the gnodes with nodes  
%
% Benedict Borer 04/2017
% BB, 04/2017, Created file


degree=full(sum(adj,2));

% GNODE CALCULATION
% Create the vertices coordinates and connectivity to the nodes
if ndim==1 && ncorners>0 && isequal(fix(ncorners),ncorners)
    % Pores in 3D space
    nvertices=degree;
    gnodeOffset=sideLength/2;
elseif ndim==2 && ncorners==4
    % 2D rectangles
    nvertices=ones(npts,1)*4;
    gnodeOffset=sqrt(2)*sideLength/2;
elseif ndim==2 && ncorners==6
    % 2D hexagons
    nvertices=ones(npts,1)*6;
    gnodeOffset=sideLength;
else
    error('Could not find the combination of dimensions with number of corners')
end

% Calculate the new coordinates of the gpts
gpts=[];
for jj=1:npts
    addingPts=getGeometryNode_FBAIBM(jj, X, Y, Z, adj, ndim, ncorners, nvertices, sideLength);
    gpts=[gpts; addingPts];
end


% Find all double points with same coordinates
ind=0;
counter=1;
while ind==0
    nodeX=gpts(counter,1);
    nodeY=gpts(counter,2);
    nodeZ=gpts(counter,3);
    distance=sqrt((nodeX-gpts(:,1)).^2+(nodeY-gpts(:,2)).^2+(nodeZ-gpts(:,3)).^2);
    % Find other nodes that are less than 1% of a sidelength in distance
    [indicator]=find(distance<0.01*sideLength);
    removal=indicator(indicator~=counter);
    gpts(removal,:)=[];
    counter=counter+1;
    if counter>=length(gpts(:,1))
       ind=1; 
    end
end

% Create the adjacency matrix 
ngpts=length(gpts(:,1));
gadj = sparse(ngpts,npts);
for kk=1:npts
    nodeX=X(kk);
    nodeY=Y(kk);
    nodeZ=Z(kk);
    distance=sqrt((nodeX-gpts(:,1)).^2+(nodeY-gpts(:,2)).^2+(nodeZ-gpts(:,3)).^2);
    indices=find(distance<1.01*gnodeOffset);
    gadj(indices,kk)=1;
end


x=gpts(:,1);
y=gpts(:,2);
z=gpts(:,3);

end

