function [ x,y,z,cadj ] = calculateConnectionNodes_FBAIBM(X,Y,Z,adj)
% Calculates the position and connectivity of geometry nodes used to assign
% bacteria and for plotting
%
%
%INPUT
% X             X Coordinates of nodes
% Y             Y Coordinates of nodes
% Z             Z Coordinates of nodes
%
%OPTIONAL INPUT
% []            No optional input required  
%                
%OUTPUTS
% x             x coordinates of connection nodes
% y             x coordinates of connection nodes
% z             x coordinates of connection nodes
% cadj          Adjacency matrix of the connection nodes with nodes  
%
% Benedict Borer 04/2017
% BB, 04/2017, Created file


%Calculate the X,Y and Z for the links and the new adjacency
npts=length(X);
[r,c,v]=find(triu(adj));
nlinks=length(r);

% Create the vectors (preassign)
x=zeros(nlinks,1);
y=zeros(nlinks,1);
z=zeros(nlinks,1);

toAdj1=zeros(nlinks,1);
toAdj2=zeros(nlinks,1);
connectionAdj=ones(2*nlinks,1);

for kk=1:nlinks
    % Calculate the x,y and z coordinates being between two nodes
   x(kk)=(X(r(kk))+X(c(kk)))/2; 
   y(kk)=(Y(r(kk))+Y(c(kk)))/2;
   z(kk)=(Z(r(kk))+Z(c(kk)))/2;
   
   % Add a connection between the two points
   toAdj1(kk)=r(kk);
   toAdj2(kk)=c(kk);
end

cadj=sparse([1:nlinks,1:nlinks]',[toAdj1;toAdj2],connectionAdj,nlinks,npts);

end

