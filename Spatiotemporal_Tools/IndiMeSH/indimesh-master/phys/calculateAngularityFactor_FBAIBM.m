function [ angularityFactor ] = calculateAngularityFactor_FBAIBM(prepAngle)
% Calculates the angularity factor based on the number of corners
%
%INPUT
% prepAngle         Matrix (nlinks x ncorners) containing the angles of each
%                   channel (in �, not radians!)
%
%OPTIONAL INPUT
% []                No optional input required  
%                
%OUTPUTS
% angularityFactor  Angularity factor (-)  
%
% Benedict Borer 05/2017
% BB, 05/2017, Created file

individualFactor=(1./tand(prepAngle./2))-(pi.*(180-prepAngle)/360);
angularityFactor=sum(individualFactor,2);
end

