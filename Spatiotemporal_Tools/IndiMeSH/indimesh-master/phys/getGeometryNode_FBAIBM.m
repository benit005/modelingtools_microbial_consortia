function [ newPts ] = getGeometryNode_FBAIBM( jj,X,Y,Z,adj, ndim, ncorners, nvertices, sideLength )
% Calculate the coordinates of new gpts depending on the desired geometry
% and location of node
%
%
%INPUT
% jj            Index of current node
% X             X Coordinates of nodes
% Y             Y Coordinates of nodes
% Z             Z Coordinates of nodes
% adj           Adjacency matrix of nodes
% npts          Number of nodes
% nvertices     Number of gnodes connected to nodes
% ndim          Dimensionality of nodes
% ncorners      Number of corners (specifies geometry)
% sideLength    Distance between nodes / node and cnode
%
%OPTIONAL INPUT
% []            No optional input required  
%                
%OUTPUTS
% newPts        Coordinates of the new gpts to add
%
% Benedict Borer 04/2017
% BB, 04/2017, Created file

nodeX=X(jj);
nodeY=Y(jj);
nodeZ=Z(jj);


if ndim==1 && ncorners>0 && isequal(fix(ncorners),ncorners)
    % Pores in 3D space
    % 1D channels
    numpts=nvertices(jj);
    connected=find(adj(:,jj));
    
    newPts=zeros(numpts,3);
    for kk=1:numpts
        newPts(kk,1)=(nodeX+X(connected(kk)))/2;
        newPts(kk,2)=(nodeY+Y(connected(kk)))/2;
        newPts(kk,3)=(nodeZ+Z(connected(kk)))/2;
    end
elseif ndim==2 && ncorners==4
    % 2D rectangles
    numpts=nvertices(jj);
    offset=sideLength/2;
    newPts=zeros(numpts,3);
    xprep=[offset offset -offset -offset]';
    yprep=[offset -offset -offset offset]';
    
    newPts(:,1)=nodeX+xprep;
    newPts(:,2)=nodeY+yprep;
    newPts(:,3)=nodeZ;
elseif ndim==2 && ncorners==6
    % 2D hexagons
    numpts=nvertices(jj);
    offset=sqrt(3)*sideLength/2;
    xprep=[0 offset offset 0 -offset -offset]';
    yprep=[sideLength sideLength/2 -sideLength/2 -sideLength -sideLength/2 sideLength/2]';

    newPts=zeros(numpts,3);
    newPts(:,1)=nodeX+xprep;
    newPts(:,2)=nodeY+yprep;
    newPts(:,3)=nodeZ;
elseif ndim==3 && ncorners==8
    % 3D cubes
    numpts=nvertices(jj);
    offset=sideLength/2;
    xprep=[offset offset -offset -offset offset offset -offset -offset]';
    yprep=[offset -offset -offset offset offset -offset -offset offset]';
    zprep=[offset offset offset offset -offset -offset -offset -offset]';

    newPts=zeros(numpts,3);
    newPts(:,1)=nodeX+xprep;
    newPts(:,2)=nodeY+yprep;
    newPts(:,3)=nodeZ+zprep;
elseif ndim==3 && ncorners==12
    % 3D rhombic dodecahedron
    
    % Srsly no clue yet.... calculate m8t
    nvertices=ones(npts,1)*14; 

else
    error('Could not find the combination of dimensions with number of corners')
end





end

