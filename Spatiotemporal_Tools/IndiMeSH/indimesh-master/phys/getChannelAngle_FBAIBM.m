function [ angle1, angle2 ] = getChannelAngle_FBAIBM( changeX,changeY,changeZ )
% Returns the angle of the channel depending on the change in X and change
% in Y, in future, needs to be generalised for different networks
%
%INPUT
% changeX       Change in X from node 1 to node 2 [1 x 1]
% changeY       Change in Y from node 1 to node 2 [1 x 1]
% changeZ       Change in Z from node 1 to node 2 [1 x 1]
%
%OPTIONAL INPUT
% []            No optional input required  
%                
%OUTPUTS
% angle1        angle in horizontal space
% angle2        angle in vertical space
%
% Benedict Borer 10/2016
% BB, 10/2016, Created file
% BB, 05/2017, Changed file to include vertical space


% Get angle in the horizontal plane, as degree and rounded
angle1=atan2(changeY,changeX);
angle1(angle1<0)=2*pi+angle1(angle1<0);
degangle=round(angle1.*180./pi);
angle1=degangle.*pi./180;

% Get angle in the vertical plane, as degree and rounded
alpha=sqrt(changeX.^2+changeY.^2);
angle2=atan2(changeZ,alpha);
angle2(angle2<0)=2*pi+angle2(angle2<0);
degangle=round(angle2.*180./pi);
angle2=degangle.*pi./180;
end

