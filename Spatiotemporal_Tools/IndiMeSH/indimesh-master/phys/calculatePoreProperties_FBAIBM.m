function [radius,centerAngle,upperAngle,height,width,sideLength ] = calculatePoreProperties_FBAIBM( dimension1,dimension2,ncorners )
% Calculates the median distance between connected points
%
%
%INPUT
% property1             Coordinates of the nodes [X Y]
% property2             Adjacency matrix [npts x npts]
%
%OPTIONAL INPUT
% []                    No optional input required
%
%OUTPUTS
% radius                Vector containing the length of each channel [nlnks x 1]
% centerAngle           Central angle
% upperAngle            Upper angle
% height                Channgel height
% width                 Channel width
% sideLength            Side length (isoscele,height)
% 
%
% Benedict Borer 09/2017
% BB, 09/2017, Created file


if ncorners==3
    radius=dimension1;
    centerAngle=dimension2;
    height=(radius./sind(centerAngle./2))+radius;
    sideLength=height./cosd(centerAngle./2);
    width=2.*(sind(centerAngle./2).*sideLength);
    upperAngle=(180-centerAngle)./2;
elseif ncorners==4
    width=dimension1;
    height=dimension2;
    radius=min(dimension1,dimension2)./2;
    centerAngle=ones(size(dimension1)).*90;
    upperAngle=ones(size(dimension1)).*90;
    sideLength=height;
end
end

