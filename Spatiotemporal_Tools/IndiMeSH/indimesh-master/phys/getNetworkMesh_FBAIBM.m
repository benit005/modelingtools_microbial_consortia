function [channelLength ] = getNetworkMesh_FBAIBM( X,Y,adj )
% Calculates the median distance between connected points 
%
%
%INPUT
% pts               Coordinates of the nodes [X Y]
% adj               Adjacency matrix [npts x npts]
%
%OPTIONAL INPUT
% []                No optional input required  
%                
%OUTPUTS
% channelLength     Vector containing the length of each channel [nlnks x 1]
%
% Benedict Borer 10/2016
% BB, 10/2016, Created file
% BB, 08/2018, Changed the calculation of mesh due to large networks (Out Of Memory)


[r,c,v]=find(triu(adj));

Xdist=round((X(r)-X(c))*10^6)./10^6;
Ydist=round((Y(r)-Y(c))*10^6)./10^6;

channelLength=sqrt(Xdist.^2+Ydist.^2);




% 
% [XX1,XX2]=meshgrid(X,X);
% x_dist=abs((XX1-XX2).*adj);
% 
% [YY1,YY2]=meshgrid(Y,Y);
% y_dist=abs((YY1-YY2).*adj);
% 
% % Euclidean distance between two points
% euclid=sqrt(x_dist.^2+y_dist.^2);
% 
% % get the x distance
% [r,c,channelLength]=find(euclid);


% Round the values to the next micrometer
channelLength=unique(round(channelLength*10^6)./10^6);

% Check the output, especielly the size
assert(length(channelLength)==1,['Resulted in non unique channel length in ' mfilename])
end

