function [ datetimeString ] = getDateTimeString_FBAIBM()
%Returns a string containing the current date and time separated by
%underlines to save current results and the log book
%
%
%INPUT
%[]             No input required          
%
%OPTIONAL INPUT
%[]             No input required
%                
%OUTPUTS
%datetime       String containing the current date and time  
%
% Benedict Borer 10/2016
% BB, 10/2016, Created file

datetimeString = datestr(now,'yyyy_mm_dd_HH_MM');
% datetimeString = datestr(now,30);
end

