function [ fileName ] = getFileNames_FBAIBM( folder, keyword, simulationID )
%[ fileName ] = getFileNames_FBAIBM( folder, keyword, simulationID )
% Obtain file name from folder depending on simulation number and a keyword
%
%
%INPUT
% folder            Folder which is searched through
% keyword           Keyword that is searched for
% simulationID      Integer number of simulation
%
%OPTIONAL INPUT
% []                No optional input required
%
%OUTPUTS
% fileName          Path to the file to be read


% Benedict Borer 11/2016
% BB, 11/2016, Created file

allFiles=dir(folder);

fileName=[];
for kk=1:length(allFiles)
    if isempty(regexpi(allFiles(kk).name,[keyword num2str(simulationID) '.txt']))==0
        fileName=allFiles(kk).name;
    end
end



if isempty(fileName)
   fileName=[keyword '.txt']; 
end

end



