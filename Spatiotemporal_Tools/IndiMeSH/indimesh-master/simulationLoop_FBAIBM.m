function [ handles ] = simulationLoop_FBAIBM( handles )
% Main simulation loop in time. Takes the following steps:
%       - update diffusion scheme
%       - diffuse substrates and consume (reaction term)
%       - calculate bacterial growth and consumption (for next time step)
%       - bacterial cell division
%       - bacterial cell death
%
%
%INPUT
% handles       FBA-IB model struct                  
%
%OPTIONAL INPUT
% []            No optional input required  
%                
%OUTPUTS
% handles       FBA-IB model struct  
%
% Benedict Borer 10/2016
% BB, 10/2016, Created file



time=zeros(handles.temp.Nt,1);

for n=1:handles.temp.Nt
    
    display(['ITERATION ' num2str(n)])
    tm=tic;
    
    handles.temp.iteration=n;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Calculation of chemical diffusion for all substrates
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if n~=1 && handles.hydro.psi(n)~=handles.hydro.psi(n-1)
        [ handles ] = updateHydrology_FBAIBM(handles);
    end
    [ handles ] = chemicalDiffusion_FBAIBM(handles);
    [ handles ] = bacterialMotility_FBAIBM(handles);
    [ handles ] = bacterialGrowth_FBAIBM(handles);
    [ handles ] = bacterialDivision_FBAIBM(handles);
    [ handles ] = bacterialDeath_FBAIBM(handles);

    handles.temp.t=handles.temp.t+handles.temp.dt;
    
    
    % Update observation vectors
    handles.obs.totalCells(n)=handles.bio.nbac;
    handles.obs.totalMass(n)=handles.bio.totBacMass;
    handles.obs.totalMassgrowth(n)=handles.bio.totBacGain;
    
    % Store the video data every video time step
    if mod(handles.temp.t,handles.temp.dTVideo)==0 && handles.adm.videoOption~=0
        [ handles ] = storeVideoData_FBAIBM( handles );
    end
    
    
    % Store all the data depending on the save interval
    if mod(handles.temp.t,handles.temp.saveInterval)==0 
        cd(handles.paths.outputPath)
        concfilename = [handles.adm.saveStump num2str(handles.temp.t./60,'%02d')];
        save(concfilename)
        cd(handles.paths.modelPath)
    end
    time(n)=toc(tm);
end

handles.time=time;

end

