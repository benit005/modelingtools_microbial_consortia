function [ handles ] = storeVideoData_FBAIBM( handles )
% Store spatial information on chemical and bacterial distribution
%
%
%INPUT
% handles       FBA-IB model struct
%
%OPTIONAL INPUT
% []            No optional input required
%
%OUTPUTS
% handles       FBA-IB model struct
%
% Benedict Borer 10/2016
% BB, 10/2016, Created file

% for ns=1:handles.bio.nspecies
%     currentMat=handles.video.speciesDistribution{ns};
%     iteration=round(handles.temp.t./handles.temp.dTVideo);
%     bac=handles.bio.species{ns};
%     currentMat(:,iteration)=distributeMassOntoGrid_FBAIBM(bac.pos,bac.m,handles.geom.npts);
%     handles.video.speciesDistribution{ns}=currentMat;
% end

iteration=round(handles.temp.t./handles.temp.dTVideo);
% bac=handles.bio.species{1};
% [observed,~]=size(handles.video.inocLoc);
% currentMat=distributeMassOntoGrid_FBAIBM(bac.pos,bac.m,handles.geom.npts);
% 
% handles.video.inocLoc(:,iteration)=bac.pos(1:observed);
% handles.video.inocC(:,iteration)=handles.chem.concWater(bac.pos(1:observed),1);
% handles.video.inocO(:,iteration)=handles.chem.concWater(bac.pos(1:observed),2);
% handles.video.inocDensity(:,iteration)=currentMat(bac.pos(1:observed));
% handles.video.weight(:,iteration)=bac.m(1:observed);
% handles.video.grate(:,iteration)=handles.bio.grate(bac.pos(1:observed));

handles.video.saturation(:,iteration)=handles.hydro.saturation;
handles.video.volumeWater(:,iteration)=handles.hydro.volumeWater;
handles.video.oxygen(:,iteration)=handles.chem.concWater(:,1);
end

