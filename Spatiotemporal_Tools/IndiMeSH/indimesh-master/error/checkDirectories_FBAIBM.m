function [  ] = checkDirectories_FBAIBM( handles )
%[  ] = checkDirectories_FBAIBM( pathCell )
% Checks all paths within the model structure
% if a path is invalid, it is enetered to the log book and the model shuts
% down
%
%
%INPUT
% handles        FBA-IB model struct 
%
%OPTIONAL INPUT
% []             No input required
%                
%OUTPUTS
% []             No output required  
%
% Benedict Borer 10/2016
% BB, 10/2016, Created file


pathCell=struct2cell(handles.paths);

for kk=1:numel(pathCell)
    if isdir(pathCell{kk})==0
        abortFunction=mfilename();
        errorString=[pathCell{kk} ' is not a valid directory'];
        addLogBookEntry_FBAIBM( handles.adm.logbook, errorString, abortFunction );
        error('Directory was not valid: Check logbook');
    end
end

end

