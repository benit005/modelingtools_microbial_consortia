function [ logbookName ] = createLogBook_FBAIBM( SimulationName )
%[  ] = createLogBook_FBAIBM(  )
%Creates a log book at the current directory for registering errors and
%warnings in future
%
%
%INPUT
%[]             No input required          
%
%OPTIONAL INPUT
%[]             No input required
%                
%OUTPUTS
%logbookName    String containing the logbook file for further writings 
%
% Benedict Borer 10/2016
% BB, 10/2016, Created file

% Get the date and time as string
timeStamp=getDateTimeString_FBAIBM();

% Name of the logbook, saved for further use
logbookName=[timeStamp '_' SimulationName  '_LogBook.txt'];


% Open logbook, enter header
fileID = fopen(logbookName,'w');
fprintf(fileID,'FBA-IB model logbook: \n');
fprintf(fileID,'Simulation started on: \t');
fprintf(fileID,'Simulation started on: \n');
fclose(fileID);


end

