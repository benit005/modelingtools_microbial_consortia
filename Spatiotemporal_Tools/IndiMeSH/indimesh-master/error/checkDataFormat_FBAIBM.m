function [  ] = checkDataFormat_FBAIBM( logbookPath,data,callerFunction,varargin)
%[  ] = checkDataFormat_FBAIBM( handles,data,callerFunction,varargin)
% Checks if the input file exists, throws an exception if not
%
%INPUT
% handles           FBA-IB model struct
% fileName          Data to be checked
% callerFunction    Name of the calling m-File
% varargin          expected dimensions (separated by commas) of the data
%
%OPTIONAL INPUT
% []                No optional input required
%
%OUTPUTS
% []                No output required
%
% Benedict Borer 10/2016
% BB, 10/2016, Created file

dim=size(data);
if isequal(dim,cell2mat(varargin))==0
    errorString=['File does not have the expected data format!'];
    addLogBookEntry_FBAIBM( logbookPath, errorString, callerFunction );
    error('Loading data not as expected: Check logbook');
end
end

