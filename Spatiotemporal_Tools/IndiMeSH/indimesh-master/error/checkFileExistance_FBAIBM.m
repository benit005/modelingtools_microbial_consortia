function [ ] = checkFileExistance_FBAIBM( logbookPath,fileName, callerFunction )
%[ ] = checkFileExistance_FBAIBM( handles,fileName, callerFunction )
% Checks if the input file exists, throws an exception if not
%
%INPUT
% handles           FBA-IB model struct 
% fileName          Path of the file of interest
% callerFunction    Name of the calling m-File
%
%OPTIONAL INPUT
% []                No optional input required  
%                
%OUTPUTS
% []                No output required
%
% Benedict Borer 10/2016
% BB, 10/2016, Created file

if exist(fileName,'file')~=2
    errorString=[fileName ' is not a valid file'];
    addLogBookEntry_FBAIBM( logbookPath, errorString, callerFunction );
    error('File path was not valid: Check logbook');
end

end

