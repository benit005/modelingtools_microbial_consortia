function [] = addLogBookEntry_FBAIBM( logbookPath, errorString, abortFunction )
%[] = addLogBookEntry_FBAIBM( handles, errorString, abortFunction )
% appends a line to the log book, including origin of the error and custom
% error text
%
%
%INPUT
%handles        FBA-IB model structure
%errorString    String containing the error message
%
%OPTIONAL INPUT
%abortFunction  String, containing the name of the warning throwing
%               function
%                
%OUTPUTS
%[]             No output required
%
% Benedict Borer 10/2016
% BB, 10/2016, Created file

if nargin<3
    abortFunction='Default Function';
end

if exist(logbookPath, 'file') ~= 2
  error('Log book has not been initialised');
end
    
dateString=getDateTimeString_FBAIBM();
messagestring=[dateString ': function ' abortFunction ' produced: ' errorString];
    
fileID = fopen(logbookPath,'a+');
fwrite(fileID,messagestring);
fwrite(fileID,'\n');
fclose(fileID);
end

