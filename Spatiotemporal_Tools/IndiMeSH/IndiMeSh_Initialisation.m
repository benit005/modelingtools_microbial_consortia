%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% IndiMeSH: Investigation of metabolic segregation
%
% Author: Benedict Borer
% Created in Matlab Version: 9.3.0 (R2017a)
% Version 0.9b
%
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This file provides initialisation routines for all the case studies used
% in the publication by Borer et al. 2019. Please not that some of these
% simulations are large, requiring a lot of computational time if not run
% on a server.
% In addition, make sure that the simulation folders, fbaibm-master and
% Metabolic network folders are in the same location as this file. A
% runnign version of glpk is also required.
%
%
%INPUT
%[]             No input required
%
%OPTIONAL INPUT
%[]             No input required
%
%OUTPUTS
%[]             No output required
%
%
% Benedict Borer 10/2019
% BB, 10/2019, Created file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Choose one of the simulations by commenting
scenario='CometsComparison'
% scenario='Micromodels'
% scenario='GlucosePerfusion'
% scenario='StrategicSmall'
% scenario='StrategicMedium'
% scenario='StrategicLarge'

switch scenario
    case 'CometsComparison'
        % This scenario simulates two inoculations, one with a ratio of
        % 99:1 E. coli to S. enterica and one with the ratio 1:99.
        SimulationName='COMETSComparison'
        FolderName='COMETSComparison'
        NumInoculations=2;
        NumHydrations=1;
    case 'Micromodels'
        % This scenario uses the micrometric pore network of Borer et al.
        % 2018 (100% connectivity) and inoculates a total of 8 different
        % scenarios.
        SimulationName='Micromodels'
        FolderName='Micromodels'
        NumInoculations=8;
        NumHydrations=1;
    case 'GlucosePerfusion'
        % This scenario simulates the last 48h of the experiment. The
        % previous 7 days are included as initial condition. I.e. only the
        % time period after glucose addition is simulated.
        SimulationName='GlucosePerfusion'
        FolderName='GlucosePerfusion'
        NumInoculations=1;
        NumHydrations=1;
    case 'StrategicSmall'
        % This scenario uses a soil cross section with fine pores and two
        % conditions (wet and dry) with P. stutzeri growing
        SimulationName='StrategicSmall'
        FolderName='StrategicSmall'
        NumInoculations=1;
        NumHydrations=2;
    case 'StrategicMedium'
        % This scenario uses a soil cross section with medium pores and two
        % conditions (wet and dry) with P. stutzeri growing
        SimulationName='StrategicMedium'
        FolderName='StrategicMedium'
        NumInoculations=1;
        NumHydrations=2;
    case 'StrategicLarge'
        % This scenario uses a soil cross section with large pores and two
        % conditions (wet and dry) with P. stutzeri growing
        SimulationName='StrategicLarge'
        FolderName='StrategicLarge'
        NumInoculations=1;
        NumHydrations=2;
end


% ParalellOption is either Growth or Simulation
ParalellOption='none';
CoreNumbers=30;
SolverName='glpk';
videoOption=0;
pathStump=cd;


indimeshPath=fullfile(pathStump,'indimesh-master');
networkPath=fullfile(pathStump,'MetabolicNetworks');
addpath(genpath(indimeshPath)); % Add all folders and files of the fbaibm toolbox
addpath(genpath(networkPath)); % Add all folders and files of the metabolic networks


% Start the actual simulation;
startSimulations_FBAIBM(FolderName,SimulationName,NumHydrations,NumInoculations,ParalellOption,CoreNumbers,SolverName,pathStump, videoOption)

















