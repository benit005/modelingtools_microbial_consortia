Computation of Microbial Ecosystems in Time and Space
COMETS is a software platform for performing computer simulations of spatially structured microbial communities. It is based on stoichiometric modeling of the genome-scale metabolic network of individual microbial species using dynamic flux balance analysis, and on a discrete approximation of diffusion. COMETS is built and maintained by the Daniel Segre Lab at Boston University.

COMETS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Comets is developed with non-commercial use in mind and is presented as-is. To inquire about collaborations or commercial usage and development, please contact us at comets@bu.edu.

Website
The COMETS website is runcomets.org.

Documentation
Documentation on how to install and use COMETS is found at https://segrelab.github.io/comets-manual/.

Installation
COMETS can be downloaded from https://www.runcomets.org/get-started.

Toolboxes
We have developed both a Matlab and a python toolbox to interface the Comets software. How to use these toolboxes are described in the documentation.

Matlab toolbox: github.com/segrelab/comets-toolbox
Python toolbox: github.com/djbajic/COMETS-Python-Toolbox


For specifics on how to run COMETS using the E.coli/S. enterica coculture from Harcombe et al. 2014, see documentation within the script file labeled ecoli_senterica.ipynb
