# GEM Modeling Tools for Microbial Consortia Assessment

This is a repository of the scripts used to evaluate genome-scale modeling tools for microbial communities for the manuscript entitled: A Structured Evaluation of Genome-Scale Constraint-Based Modeling Tools for Microbial Consortia.

We quantitatively evaluated 14 adequate tools against published experimental data that included different organisms and conditions. We selected one dataset for each category of modeling tools, including static, dynamic and spatiotemporal tools. 

Tools from each category are available in the corresponding folder that also contains additional instructions.


