%clear all
%Cobra Toolbox v3.33
%MMT 2.0
initCobraToolbox

%% Create community model
changeCobraSolver('glpk', 'LP')
modelFolder=[pwd filesep 'modelFolder']; 
modelList = {'auto';'kluy'};
joinModelsPairwiseFromList({'auto';'kluy'},modelFolder,'pairwiseModelFolder',modelFolder); %This function generates the community model and the pairedModelinfo file
load([pwd filesep 'modelFolder' filesep 'pairedModel_auto_kluy.mat']); %pairedModel_auto_kluy.mat is the name of the community model

%% Assign bounds to the community model
Data= readtable('Bounds_MMT.xlsx'); %Assign bounds to the reactions defined in [u] compartment in the community model 
lb_com=Data{1:2419,2};
ub_com=Data{1:2419,3};
for w = 1:2419
    pairedModel = changeRxnBounds(pairedModel,pairedModel.rxns(w), lb_com(w,1), 'l');
    pairedModel = changeRxnBounds(pairedModel,pairedModel.rxns(w), ub_com(w,1), 'u'); 
end

%% Input data and constraints

pairedModel=changeRxnBounds(pairedModel, 'EX_CO[u]', -4.8, 'l'); %Define CO uptake rate (mmol/h)
pairedModel=changeRxnBounds(pairedModel, 'auto_BIOt', 0.021*0.22*0.4, 'u'); %Fixed flux through biomass reactions (growth rate* biomass of each species)
pairedModel=changeRxnBounds(pairedModel, 'kluy_BIOt',0.021*0.22*0.6, 'u');
%pairedModel=changeRxnBounds(pairedModel, 'auto_atpm', 8.4*0.22*0.4, 'l');%ATP maintenance flux scaled to mmol/h. Infeasible
%pairedModel=changeRxnBounds(pairedModel, 'kluy_Rckl725',0.45*0.22*0.6, 'l');
save([pwd filesep 'modelFolder' filesep  'pairedModel_auto_kluy.mat'],'pairedModel') %This saves the community model with the added constraints

%% Run simulatePairwiseInteractions

modPath=[pwd filesep 'modelFolder'];
[pairwiseInteractions, pairwiseSolutions] = simulatePairwiseInteractions(modPath,'saveSolutionsFlag',true); %Outputs the fluxes and growth rates
