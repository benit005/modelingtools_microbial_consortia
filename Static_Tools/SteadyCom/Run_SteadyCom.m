%COBRA Toolbox v3.33
%clear all
initCobraToolbox(false) 
% 
changeCobraSolver('glpk', 'LP') %glpk is available when installing COBRA Toolbox. Consider changing to CPLEX if FVA is used.
%% Read single GEMs

fileName='..\models\iCLAU786_SteadyCom.xml'; %Modify path accordingly
model1 = readCbModel(fileName, 'fileType','SBML');
fileName='..\models\ickl708_SteadyCom.xml';
model2 = readCbModel(fileName, 'fileType','SBML');

%% Create community model
models{1,1} =model1;
models{2,1} =model2;
nameTagsModels={'auto';'kluy'}; 
[modelJoint_CACK] = createMultipleSpeciesModel({model1;model2},nameTagsModels); %I have changed line 165 in the COBRA Toolbox script that runs this function (biomass id).
nameTagHost=[];
nameTagsModels={'auto';'kluy'};

%% Get the infoCom and indCom

[modelJoint_CACK.infoCom,modelJoint_CACK.indCom]=getMultiSpeciesModelId(modelJoint_CACK, nameTagsModels,nameTagHost); %This is changed to generate the infoCom and indCom  
disp( modelJoint_CACK.infoCom)
disp( modelJoint_CACK.indCom)
rxnBiomass = strcat(nameTagsModels, {'BIOt';'BIOt'}); % id of the biomass reactions
rxnBiomassId = findRxnIDs(modelJoint_CACK, rxnBiomass); % ids index
modelJoint_CACK.infoCom.spBm = rxnBiomass; % .spBm for organism biomass reactions
modelJoint_CACK.indCom.spBm = rxnBiomassId;
rxnATPM={'autoatpm';'kluyRckl725'}; %ATPM reactions
rxnATPMId = findRxnIDs(modelJoint_CACK, rxnATPM);  % ids
modelJoint_CACK.infoCom.spATPM = rxnATPM;  % .spBm for organism biomass reactions
modelJoint_CACK.indCom.spATPM = rxnATPMId;


%% Read excel file with reaction bounds. 
Data= readtable('Bounds_steadycom.xlsx'); %Assign bounds to the reactions defined in the community compartment [u]. Otherwise, they are all [-1000,1000] 
lb_auto=Data{1:230,3};
ub_auto=Data{1:230,4};
lb_kluy=Data{1:230,5};
ub_kluy=Data{1:230,6};
lb_com=Data{1:230,8};
ub_com=Data{1:230,9};
%%Assign bounds to the reactions
for i = 1:230
    modelJoint_CACK = changeRxnBounds(modelJoint_CACK,modelJoint_CACK.infoCom.EXsp(i,1), min(lb_auto(i,1), ub_auto(i,1)), 'l');
    modelJoint_CACK = changeRxnBounds(modelJoint_CACK,modelJoint_CACK.infoCom.EXsp(i,1), max(lb_auto(i,1), ub_auto(i,1)), 'u');
    modelJoint_CACK = changeRxnBounds(modelJoint_CACK,modelJoint_CACK.infoCom.EXsp(i,2), lb_kluy(i,1), 'l');
    modelJoint_CACK = changeRxnBounds(modelJoint_CACK,modelJoint_CACK.infoCom.EXsp(i,2), ub_kluy(i,1), 'u');
    modelJoint_CACK = changeRxnBounds(modelJoint_CACK,modelJoint_CACK.infoCom.EXcom(i,1),lb_com(i,1), 'l');
    modelJoint_CACK = changeRxnBounds(modelJoint_CACK,modelJoint_CACK.infoCom.EXcom(i,1),ub_com(i,1), 'u'); 
end
%% Model inputs and constraints

modelJoint_CACK.c(2071,1) = 1; %important to include the objectives coefficients in c.
modelJoint_CACK.c(961,1) = 1; 
modelJoint_CACK=changeRxnBounds(modelJoint_CACK, 'EX_CO[u]', -4.8, 'b'); %CO uptake rate (mmol/h)
modelJoint_CACK=changeRxnBounds(modelJoint_CACK, 'EX_H2[u]', -0, 'l');
modelJoint_CACK=changeRxnBounds(modelJoint_CACK, 'EX_FRU[u]', 0, 'b');
modelJoint_CACK=changeRxnBounds(modelJoint_CACK, 'autoBIOt', 0, 'l');
modelJoint_CACK=changeRxnBounds(modelJoint_CACK, 'autoBIOt', 1000, 'u');
modelJoint_CACK=changeRxnBounds(modelJoint_CACK, 'autoIEX_biomass[c]tr', 1000, 'u');
modelJoint_CACK=changeRxnBounds(modelJoint_CACK, 'kluyBIOt', 0, 'l');
modelJoint_CACK=changeRxnBounds(modelJoint_CACK, 'kluyBIOt', 1000, 'u');
modelJoint_CACK=changeRxnBounds(modelJoint_CACK, 'autoatpm', 8.4*0.22*0.4, 'b');
modelJoint_CACK=changeRxnBounds(modelJoint_CACK, 'kluyRckl725', 0.45*0.22*0.6, 'b');

%printUptakeBound(modelJoint_CACK);

%printFluxBounds(modelJoint_CACK);

%% Run SteadyCom

options = struct();
options.GRguess = 0.5; % initial guess for max. growth rate
% options.GRtol = 1e-06; % tolerance for final growth rate
options.algorithm = 1; % use the default algorithm (simple guessing for bounds, followed by matlab fzero)
options.feasCrit=1;
options.BMweight=0.2;
%options.BMgdw=[0.088,0.13];
[sol, result] = SteadyCom(modelJoint_CACK, options);




