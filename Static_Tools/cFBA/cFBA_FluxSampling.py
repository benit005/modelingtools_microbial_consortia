#!/usr/bin/env python

"""
Author Sara Benito Vaquerizo. 
Script to run cFBA using the method from Benito-Vaquerizo. et al.,CSBJ,2020. to model the co-culture of C. autoethanogenum and C. kluyveri
The community model is taken directly from the same publication (Multi_species GEM) without adding the additional constraints.
Cobrapy and functions are used from https://github.com/opencobra/m_model_collection/
This requires [cobrapy](https://opencobra.github.io/cobrapy) version 0.24.0
Python 3.7 is used as the programming language. GLPK is used as the default solver.
"""
# Import statements
import os
import warnings
import re
from itertools import chain
from sys import argv

import sympy
import scipy
import scipy.io

import cobra
import cobra.test

from cobra import Model, Reaction, Metabolite
import pandas
from cobra.util.solver import linear_reaction_coefficients

import numpy as np
import pandas as pd

from cobra.flux_analysis import (
    single_gene_deletion, single_reaction_deletion, double_gene_deletion,
    double_reaction_deletion)
import statistics
from cobra.medium import minimal_medium
from cobra.test import create_test_model
#import util
import multiprocessing
import multiprocessing.pool
import statistics as stats
from statistics import stdev
import optlang
import cobra.util.solver as sutil
from cobra.sampling import OptGPSampler, ACHRSampler,sample
from micom.data import test_taxonomy
from micom import Community
from micom.workflows import workflow
from micom.data import test_db
from micom.workflows import build
from micom.workflows import build_database
from micom import load_pickle
from cobra.io import read_sbml_model, save_json_model
from glob import glob
from micom.logger import logger
from micom.util import join_models, load_pickle
from micom.community import Community, _ranks
from micom.workflows.core import workflow
from micom.workflows import grow
import math
from math import sqrt
from cobra.flux_analysis.loopless import add_loopless, loopless_solution
from cobra.flux_analysis import pfba
import statistics as stats
from statistics import stdev 
	

if __name__ == "__main__":
	model=cobra.io.read_sbml_model('../models/Multi_species.xml')	#Modify path accordingly. It reads directly the community model.
	model.reactions.Biomass_trans_ckluy.lower_bound=0.021*0.22*0.6 #Biomass reaction includes the growth rate multiplied to the biomass of each species (total biomass* species ratio)
	model.reactions.Biomass_trans_ckluy.upper_bound=0.021*0.22*0.6
	model.reactions.Biomass_trans_auto.lower_bound=0.021*0.22*0.4
	model.reactions.Biomass_trans_auto.upper_bound=0.021*0.22*0.4
	model.reactions.ATPM.bounds=[0.45*0.22*0.6,0.45*0.22*0.6] #ATPM flux transformed into mmol/L (multiplied  by the biomass of each species)
	model.reactions.ATPM_auto.bounds=[8.4*0.22*0.4,8.4*0.22*0.4]
	model.reactions.EX_CO_e.lower_bound=-4.8 #Substrate uptake rate (mmol/h) 
	model.reactions.EX_CO_e.upper_bound=-4.8
	model.reactions.COt.bounds=[0,0]
	model.reactions.EX_CO2_e.lower_bound=0	
	model.reactions.EX_CO2_e.upper_bound=1000
	model.reactions.EX_H2_e.lower_bound=0
	model.reactions.EX_AC_e.lower_bound=0	
	model.reactions.EX_ETOH_e.lower_bound=0	
	model.reactions.EX_HEPT_e.bounds=[0,0]
	model.reactions.rxn05559_c0.bounds=[-0.05,0.0]
	model.reactions.EX_FOR_e.bounds=[0,0.05]	
	suma=0.0
	average=0.0
	standard_deviation=0.0
	reactions=[] #List to store the reactions
	flux=[] #List to store average fluxes
	stdev=[]# List to store stdv
	lista_samples=[]
	#Apply flux sampling to compute fluxes
	number_samples=10000 #Number of samples to run Flux sampling
	s = sample(model,number_samples, method="achr") 
	for col in s.columns:
		columnsData = s.loc[ : , col]
		suma=0
		total=0
		lista_samples=[]
		for k in range(len(columnsData)):
			suma=suma + float(columnsData[k])
			total=total+1
			lista_samples.append(float(columnsData[k]))
		average=float(round(suma/total,4))
		standard_deviation=statistics.stdev(lista_samples)
		reactions.append(col)
		flux.append(average)
		stdev.append(standard_deviation)	
	with open('fluxes_cFBA.txt', 'w') as f: #Modify path accordingly
		f.write('{}\t{}\t{}\n'.format('Reactions','average','stdev'))
		for i in range(len(reactions)):
			f.write('{}\t{}\t{}\n'.format(reactions[i],flux[i],stdev[i]))
		f.close()
	
