#!/usr/bin/env python

"""
Author Sara Benito-Vaquerizo. 
Script to run MICOM 0.32.2 to model a co-culture of C. autoethanogenum and C. kluyveri. 
First, the community model is created from single GEMs (iCLAU786 and iCKL708).
MICOM is then run to model the co-culture. 
Cobrapy and functions are used from https://github.com/opencobra/m_model_collection/
This requires [cobrapy](https://opencobra.github.io/cobrapy) version 0.24.0. 
OSQP is used as the default solver. 
Python 3.7 is used as the programming language.
"""
# Import statements
import os
import warnings
import re
from itertools import chain
from sys import argv

import sympy
import scipy
import scipy.io

from micom import Community
from glob import glob
from micom import load_pickle
import cobra

from cobra import Model, Reaction, Metabolite
import pandas
from cobra.util.solver import linear_reaction_coefficients
from micom.workflows import grow

import numpy as np
import pandas as pd
from contextlib import suppress
from cobra.medium import minimal_medium
# import util
import multiprocessing
import multiprocessing.pool
from cobra.flux_analysis import production_envelope
from cobra.sampling import OptGPSampler, ACHRSampler, sample

# from cobra.flux_analysis import sample
from cobra.flux_analysis import pfba
import optlang
import cobra.util.solver as sutil
import pandas as pd
from micom.media import minimal_medium
	
if __name__ == "__main__":
	#Define input data. Modify path accordingly
	table = {'id': ['auto', 'kluy'],'species': ['Clostridium autoethanogenum','Clostridium kluyveri'],
        'reactions':[1106,993],'metabolites':[1093,804],
        'file':['models/iCLAU786_MICOM.xml',\
        'models/ickl708_MICOM.xml'],\
        "genus":['Clostridium','Clostridium'],'abundance':[0.088,0.132]}
	data = pd.DataFrame(table)	
	taxonomy = data	
	com = Community(taxonomy,mass=0.22)
	com.reactions.EX_CO_m.lower_bound=-4.8 #Subtrate uptake rate in mmol/h
	com.reactions.EX_CO_m.upper_bound=-4.8
	com.reactions.Rckl765__kluy.bounds=[0,0]
	com.reactions.EX_PPI_m.bounds=[-1000,0]
	com.reactions.EX_CO2_m.lower_bound=0	
	com.reactions.EX_CO2_m.upper_bound=1000
	com.reactions.EX_H2_m.lower_bound=0
	com.reactions.EX_AC_m.lower_bound=0	
	com.reactions.EX_ETOH_m.lower_bound=0	
	com.reactions.EX_HEPT_m.bounds=[0,0]
	com.reactions.EX_FOR_e__auto.bounds=[0,0.5]
	com.reactions.EX_FOR_m.bounds=[0,0.05]
	
	# This is what is usually done with the mass parameter 
	#  but this does not support changing the bounds after.
	# This assumes you import bounds are provided as mmol/h (considering 1 g of total biomass)
	# so they need to be divided by the dry weight of bacteria (total community biomass)
	# in the sample to yield mmol/(gDW * h)
	med = pd.Series(com.medium)
	com.medium = med / data.abundance.sum()
	com.solver = "osqp"
	com.solver.configuration.presolve = True
	sol = com.cooperative_tradeoff(fraction=0.5, fluxes=True)
	#sol=com.optimize(fluxes=True)
	sol.fluxes *= data.abundance.sum()  # convert to mmol/h - inverse to before to get back to same unit
	print(sol)
	print(sol.members)
	#Store the fluxes
	sol.fluxes.T.to_csv('fluxes_MICOM.csv', sep='\t')

	
	
